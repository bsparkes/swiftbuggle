//
//  CoreTrieNode+CoreDataProperties.swift
//  waggle
//
//  Created by Ben Sparkes on 20/03/2020.
//  Copyright © 2020 sparkes. All rights reserved.
//
//

import Foundation
import CoreData


extension CoreTrieNode {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CoreTrieNode> {
        return NSFetchRequest<CoreTrieNode>(entityName: "CoreTrieNode")
    }

    @NSManaged public var value: String
    @NSManaged public var wordType: Int16
    @NSManaged public var children: Set<CoreTrieNode>
    @NSManaged public var parent: CoreTrieNode?

}

// MARK: Generated accessors for children
extension CoreTrieNode {

    @objc(addChildrenObject:)
    @NSManaged public func addToChildren(_ value: CoreTrieNode)

    @objc(removeChildrenObject:)
    @NSManaged public func removeFromChildren(_ value: CoreTrieNode)

    @objc(addChildren:)
    @NSManaged public func addToChildren(_ values: NSSet)

    @objc(removeChildren:)
    @NSManaged public func removeFromChildren(_ values: NSSet)

}
