//
//  StatFunctions.swift
//  buggle
//
//  Created by Benjamin Sparkes on 20/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import Foundation


func currentDate() -> String {
  // Help from https://stackoverflow.com/questions/24070450/how-to-get-the-current-time-as-datetime
  
  let currentDate = Date()
  let formatter = DateFormatter()
  formatter.timeStyle = .none
  formatter.dateStyle = .long
  
  return formatter.string(from: currentDate)
}

func updateStats(model m: GameModel) {
  
  let stats = loadStatCollection()
  var statsChanged = false
  
  if m.foundWords.count > Int(stats.mostWords.value) ?? 0 {
    print("update found words")
    stats.mostWords.value = String(m.foundWords.count)
    stats.mostWords.date = currentDate()
    stats.mostWords.board = m.abstractGameBoard
    statsChanged = true
  }
  
  if m.score > Int(stats.mostPoints.value) ?? 0 {
    print("update score")
    stats.mostPoints.value = String(m.score)
    stats.mostPoints.date = currentDate()
    stats.mostPoints.board = m.abstractGameBoard
    statsChanged = true
  }
  
  let wordsFound = m.foundWords.count
  let possibleWords = m.possibleWords.count
  let modelPercent = Double(wordsFound)/Double(possibleWords)
  
  if modelPercent > Double(stats.highestPercent.value) ?? 0 {
    stats.highestPercent.value = "\(wordsFound)/\(possibleWords)   (" + String(format: "%.2f%", (modelPercent*100)) + "%)"
    stats.highestPercent.date = currentDate()
    stats.highestPercent.board = m.abstractGameBoard
    statsChanged = true
  }
  
  if m.longestWord.count > stats.longestWord.value.count {
    stats.longestWord.value = m.longestWord
    stats.longestWord.date = currentDate()
    stats.longestWord.board = m.abstractGameBoard
    statsChanged = true
  }
  
  if m.longestHeterogram.count > stats.longestHeterogram.value.count {
    stats.longestHeterogram.value = m.longestHeterogram
    stats.longestHeterogram.date = currentDate()
    stats.longestHeterogram.board = m.abstractGameBoard
    statsChanged = true
  }

  if statsChanged {
    stats.saveStatCollection()
    NotificationCenter.default.post(name: .didUpdateStats, object: nil)
  }
  
}
