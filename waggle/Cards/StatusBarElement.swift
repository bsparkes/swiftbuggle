//
//  StatusBarElement.swift
//  buggle
//
//  Created by Benjamin Sparkes on 16/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import UIKit

class StatusBarElement : UIView {
  
  let elementName: String
  var elementValue: String
  let height: CGFloat
  let width: CGFloat
  let xPosition: CGFloat
  
  var icon: IconView?
  var status: UILabel
  
  init(elementName name: String, elementValue value: String, xPosition x: CGFloat, width w: CGFloat, height h: CGFloat) {
    
    elementName = name
    elementValue = value
    height = h
    width = w
    xPosition = x
    
    status = UILabel(frame: CGRect(x: 0, y: 0, width: w, height: h))
    
    status.backgroundColor = UIColor.clear
    status.text = value
    status.textAlignment = .center
    status.textColor = statusTextColour
    status.highlightedTextColor = UIColor.clear
    status.minimumScaleFactor = 0.5
    status.font = UIFont(name: textFontName, size: h*0.8)
    
    super.init(frame: CGRect(x: xPosition, y: 0, width: w, height: h))
    
    addSubview(status)
  }
  
  
  init(elementName name: String, elementValue value: String, xPosition x: CGFloat, width w: CGFloat, height h: CGFloat, icon i: IconView) {
    
    elementName = name
    elementValue = value
    height = h
    width = w
    xPosition = x
    icon = i
    
    status = UILabel(frame: CGRect(x: h, y: 0, width: w - h, height: h))
    
    status.backgroundColor = UIColor.clear
    status.text = value
    status.textAlignment = .center
    status.textColor = statusTextColour
    status.highlightedTextColor = UIColor.clear
    status.minimumScaleFactor = 0.5
    status.font = UIFont(name: textFontName, size: h*0.8)
    
    super.init(frame: CGRect(x: xPosition, y: 0, width: w, height: h))
    
    addSubview(icon!)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // There's either an icon or text, at least for now
  func updateElementValue(value v: Int) {
    if icon != nil {
      icon?.updateIcon(value: v)
    } else {
      status.isHighlighted = false
      status.text = String(v)
    }
  }
  
  func dullElement() {
    status.isHighlighted = true
  }
  
}
