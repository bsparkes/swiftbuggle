//
//  settingsChoiceController.swift
//  buggle
//
//  Created by Benjamin Sparkes on 19/12/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

class SettingsChoiceLabel : UILabel {
  
  private var textSize: CGFloat
  unowned var settingsController: SettingsFieldView
  private var toggleChoiceRecognizer: UITapGestureRecognizer?
  private let icon: IconView?
  private let cornerRadius: CGFloat
  
  public let value: Int
  
  init(frame f: CGRect, text t: String, value v: Int, textHeight h : CGFloat, cornerRadius cR: CGFloat, controller c : SettingsFieldView, icon i: IconView?) {
    
    value = v
    textSize = h
    settingsController = c
    icon = i
    cornerRadius = cR
    
    super.init(frame: f)
    
    textAlignment = .center
    layer.masksToBounds = true
    layer.cornerRadius = cR
    
    if i != nil {
      attributedText = makeText(text: t, show: false)
    } else {
      attributedText = makeText(text: t, show: true)
    }
    
    isUserInteractionEnabled = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  func addHighlight() {
//    layer.masksToBounds = false
    let shapeLayer:CAShapeLayer = CAShapeLayer()
    let frameSize = self.frame.size
    let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
    
    let path = UIBezierPath(roundedRect: shapeRect, cornerRadius: cornerRadius)

    shapeLayer.path = path.cgPath
    shapeLayer.masksToBounds = true
    shapeLayer.bounds = shapeRect
    shapeLayer.cornerRadius = cornerRadius
    shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.strokeColor = settingsTextColour.cgColor
    shapeLayer.lineWidth = 2
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round
    shapeLayer.lineDashPattern = [6,3]

    layer.addSublayer(shapeLayer)
  }
  
  
  func removeHighlight() {
    
    layer.masksToBounds = true
    layer.sublayers?.removeAll()
  }
  
  
  private func makeText(text t : String, show s: Bool) -> NSMutableAttributedString {
    
    let textColour = s ? settingsTextColour : UIColor.clear
    
    let choiceAttributes = [
      NSAttributedString.Key.strokeColor : textColour,
      NSAttributedString.Key.foregroundColor : textColour,
      //        NSAttributedString.Key.strokeWidth : -1,
      NSAttributedString.Key.font : UIFont(name: textFontName, size: textSize)!
      ] as [NSAttributedString.Key : Any]
    
    if icon != nil {
      addSubview(icon!)
      if icon!.layer.sublayers?.count != 0 {
        for lay in icon!.layer.sublayers! as! [CAShapeLayer] {
          lay.strokeColor = settingsTextColour.cgColor
        }
      }
    }
    return NSMutableAttributedString(string: t, attributes: choiceAttributes)
  }
  
  
  // Some choice, e.g. 'inf' for time require an icon, hence the cases here.
  func selectChoiceUI() {
    
    backgroundColor = settingsTextColour
    
    if icon == nil {
      let choiceAttributes = [
        NSAttributedString.Key.strokeColor : settingsTextSelectedColour,
        NSAttributedString.Key.foregroundColor : settingsTextSelectedColour,
        NSAttributedString.Key.strokeWidth : -1,
        NSAttributedString.Key.font : UIFont(name: textFontName, size: textSize)!
        ] as [NSAttributedString.Key : Any]
      let cchoice = NSMutableAttributedString(string: text!, attributes: choiceAttributes)
      attributedText = cchoice
    } else {
      addSubview(icon!)
      if icon!.layer.sublayers?.count != 0 {
        for lay in icon!.layer.sublayers! as! [CAShapeLayer] {
          lay.strokeColor = settingsTextSelectedColour.cgColor
        }
      }
    }
  }
  
  // Change the background and colour of text on selection
  func selectChoice() {
    settingsController.updateCurrentSetting(newChoice: self)
    selectChoiceUI()
    removeToggle()
  }
  
  
  func deselectChoiceUI() {
    
    backgroundColor = settingsMenuBackgroundColour
    
    if icon == nil {
      let choiceAttributes = [
        NSAttributedString.Key.strokeColor : settingsTextColour,
        NSAttributedString.Key.foregroundColor : settingsTextColour,
        NSAttributedString.Key.strokeWidth : -1,
        NSAttributedString.Key.font : UIFont(name: textFontName, size: textSize)!
        ] as [NSAttributedString.Key : Any]
      let cchoice = NSMutableAttributedString(string: text!, attributes: choiceAttributes)
      attributedText = cchoice
    } else {
      addSubview(icon!)
      if icon!.layer.sublayers?.count != 0 {
        for lay in icon!.layer.sublayers! as! [CAShapeLayer] {
          lay.strokeColor = settingsTextColour.cgColor
        }
      }
    }
  }
  
  // change the background and colour of text on deselection
  func deselectChoice() {
    addToggle()
    deselectChoiceUI()
  }
  
  
  // add a toggle tracking selection of different menus
  func addToggle() {
    //    print("added toggle", self.text as Any)
    toggleChoiceRecognizer = UITapGestureRecognizer(target: self, action: #selector(toggleChoice))
    addGestureRecognizer(toggleChoiceRecognizer!)
  }
  
  func removeToggle() {
    //    print("removing choice toggle:", self.text as Any)
    if toggleChoiceRecognizer != nil {
      //      print("toggle recognizer found")
      removeGestureRecognizer(toggleChoiceRecognizer!)
    }
  }
  
  // update the particular choice
  @objc func toggleChoice(_ r: UIGestureRecognizer) {
    selectChoice()
  }
  
}


