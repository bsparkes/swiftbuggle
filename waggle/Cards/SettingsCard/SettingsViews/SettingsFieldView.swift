//
//  SettingsFieldView.swift
//  buggle
//
//  Created by Benjamin Sparkes on 15/05/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import UIKit


class SettingsFieldView: UIView {
  
  private let statusBarWidth: CGFloat
  private let statusBarHeight: CGFloat
  private let settingsText: String
  
  private let xPosition: CGFloat
  private let yPosition: CGFloat
  private let choiceSperatorWidth : CGFloat
  
  private var maxChoiceWidth = CGFloat(0)
  private var menuWidth = CGFloat(0)
  private var menuOffset = CGFloat(0)
  
  private let configIdentifier: String
  private let choiceValues: [Int]
  private var choices = [SettingsChoiceLabel]()
  private var currentChoice = 0
  private var fixedChoice = 0
  
  private let cornerRadius: CGFloat
  
  init(cardWidth width: CGFloat, cardHeight height: CGFloat, yPosition yPos: CGFloat, configItem n: String, choices c: [Int], text t: String) {
    
    statusBarWidth = width*0.9
    statusBarHeight = height*0.07
    settingsText = t
    xPosition = width*0.05
    yPosition = yPos
    choiceSperatorWidth = statusBarHeight * 0.1
    configIdentifier = n
    choiceValues = c
    cornerRadius = getCornerRadius(width: statusBarWidth)
    
    super.init(frame: CGRect(x: xPosition, y: yPosition, width: statusBarWidth, height: statusBarHeight))
    
    sharedSetup()
  }
  
  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  func sharedSetup() {
    layer.cornerRadius = getCornerRadius(width: statusBarWidth)
    
    addSettingsText(labelText: settingsText, labelWidth: statusBarWidth, xPosition: 0.05)
    
    maxChoiceWidth = determineChoiceWidth()
    menuWidth = (maxChoiceWidth * CGFloat(choiceValues.count)) + (choiceSperatorWidth * CGFloat(choiceValues.count + 1))
    menuOffset = getMenuOffset()
    makeMenuBackground()
    populateChoices()
  }
  
  
  func updateCurrentSetting(newChoice: SettingsChoiceLabel) {
    global.gameConfig.setOption(identifier: configIdentifier, value: newChoice.value)
    updateCurrentSettingBackground(newChoice: newChoice)
  }
  
  
  func updateCurrentSettingBackground(newChoice: SettingsChoiceLabel) -> Void {
    choices[currentChoice].deselectChoice()
    currentChoice = choices.lastIndex(of: newChoice)!
  }
  
  // Go through the choices and display them, but also highlight the current choice
  private func populateChoices() {
    
    var xOffset = menuOffset + choiceSperatorWidth
    let existingChoiceValue = global.gameConfig.getCurrentOption(option: configIdentifier, new: false)
    let newChoiceValue = global.gameConfig.getCurrentOption(option: configIdentifier, new: true)
    
    for menuNum in 0 ..< choiceValues.count {
      
      let choiceValue = choiceValues[menuNum]
      
      let choiceText = global.gameConfig.getOptionString(identifier: configIdentifier, value: choiceValue)
      var choiceLabel = SettingsChoiceLabel(frame: CGRect(x: xOffset,
                                                          y: 0,
                                                          width: maxChoiceWidth,
                                                          height: statusBarHeight),
                                            text: choiceText,
                                            value: choiceValue,
                                            textHeight: statusBarHeight*0.8,
                                            cornerRadius: cornerRadius,
                                            controller: self,
                                            icon: nil)
      if choiceText == "-1" {
        choiceLabel = SettingsChoiceLabel(frame: CGRect(x: xOffset,
                                                        y: 0,
                                                        width: maxChoiceWidth,
                                                        height: statusBarHeight),
                                          text: choiceText,
                                          value: choiceValue,
                                          textHeight: statusBarHeight*0.8,
                                          cornerRadius: cornerRadius,
                                          controller: self,
                                          icon: infIcon(size: maxChoiceWidth))
      }
      
      choices.append(choiceLabel)
      addSubview(choiceLabel)
      xOffset += maxChoiceWidth + choiceSperatorWidth
      
      if choiceValue == existingChoiceValue {
          fixedChoice = menuNum
          choiceLabel.addHighlight()
      }
      
      if choiceValue == newChoiceValue {
        currentChoice = menuNum
        choiceLabel.selectChoiceUI()
      } else {
        choiceLabel.addToggle()
      }
    }
  }
  
  
  private func makeMenuBackground() {
    
    let background = UIView(frame: CGRect(x: menuOffset,
                                          y: -statusBarHeight * 0.1,
                                          width: menuWidth,
                                          height: statusBarHeight * 1.2))
    background.layer.masksToBounds = true
    background.layer.cornerRadius = cornerRadius
    background.backgroundColor = settingsMenuBackgroundColour
    addSubview(background)
  }
  
  
  private func getMenuOffset() -> CGFloat {
    return statusBarWidth - menuWidth
  }
  
  
  private func addSettingsText(labelText: String, labelWidth: CGFloat, xPosition: CGFloat) {
    
    let label = UILabel(frame: CGRect(x: xPosition,
                                      y: 0,
                                      width: labelWidth,
                                      height: statusBarHeight))
    label.text = labelText
    label.textColor = settingsMenuBackgroundColour
    label.textAlignment = NSTextAlignment.left
    label.minimumScaleFactor = 0.5
    
    label.font = UIFont(name: textFontName, size: statusBarHeight*0.8)
    
    addSubview(label)
  }
  
  
  private func determineChoiceWidth() -> CGFloat {
    
    var maxChoiceWidth = CGFloat(0)
    for choice in choiceValues {
      
      let choiceText = global.gameConfig.getOptionString(identifier: configIdentifier, value: choice)
      
      let choiceWidth = getLabelWidth(boundingHeight: statusBarHeight,
                                      font: UIFont(name: textFontName, size: statusBarHeight*0.8)!,
                                      labelText: choiceText)
      if (choiceWidth > maxChoiceWidth) {
        maxChoiceWidth = choiceWidth
      }
    }
    return max(maxChoiceWidth * 1.2, statusBarHeight)
  }
  
  
  private func getLabelWidth(boundingHeight height: CGFloat, font: UIFont, labelText t: String) -> CGFloat {
    
    let tt = t as NSString
    let constraingRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingRect = tt.boundingRect(with: constraingRect,
                                       options: .usesLineFragmentOrigin,
                                       attributes: [.font: font],
                                       context: nil)
    return ceil(boundingRect.width)
  }
  
}
