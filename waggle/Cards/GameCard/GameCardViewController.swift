//
//  cardgameboardController.swift
//  buggle
//
//  Created by Benjamin Sparkes on 29/06/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

class GameCardViewController: CardViewController {
  
  var gamePaused: Bool = true
  
  // For the game
  var gameTimer: Timer?
  
  // UI Elements
  var board: GameboardViewController?
  var stopwatch: StopwatchView?
  var reset: ResetView?
  var foundWordView: FoundWordView!
  
  // UI Gesture recognisers
  var tileInteraction: UIPanGestureRecognizer?
  var timerTapInteraction: UITapGestureRecognizer?
  var resetTapInteraction: UITapGestureRecognizer?
  
  // Notification observers
  private var scoreObserver: Any!
  
  // UI Stuff
  let boardCornerRadius: CGFloat
  
  
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(cardName name: String, cardWidth width: CGFloat, cardColour colour: CGColor) {
    
    boardCornerRadius = getCornerRadius(width: width * 0.95)
    
    super.init(cardName: name, cardWidth: width, cardColour: colour)
    
    card.statusBar.addElement(elementName: "numFound", elementValue: "0", labelWidth: card.statusBar.frame.width/2, xPosition: 0)
    card.statusBar.addElement(elementName: "score", elementValue: "0", labelWidth: card.statusBar.frame.width/2, xPosition: card.statusBar.frame.width/2)
    card.statusBar.addLabelsToView()
    addScoreObserver()
    
    buildCardBasics()
    setupControls()
    
    board = GameboardViewController(gameModel: global.gameModel, boardSize: width * 0.95)
    board!.addToView(view: card)

    
    // Add a couple of checks to see if a game was in progress
    if (Double(global.gameModel.time) != global.gameModel.timeRemaining) {
      pauseGame(animated: false)
      foundWordView.listUpdateAndScroll(updateList: global.gameModel.foundWords)
      addResetButton()
    }
    stopwatch?.setHandPosition()
  }
  
  func newGame(start: Bool) {
    
    global.gameModel = global.gameConfig.makeGameModel()
    board = GameboardViewController(gameModel: global.gameModel, boardSize: cardWidth * 0.95)
    board!.addToView(view: card)
    stopwatch?.displayHandOrInf()
    addScoreObserver()
    global.gameModel.completeWordSetBackground()
    if start {
      unpauseGame(animated: true)
    }
  }
  
  func clearOldGame() {
    board?.removeSubViews()
    foundWordView.clearWords()
    foundWordView.reloadData()
    global.gameConfig.refreshConfig()
    stopwatch?.resetTimer()
  }
  
  func gameOver() {
    gamePaused = true
    global.gameModel.timeRemaining = 0
    removeResetButton()
    gameTimer?.invalidate()
    stopwatch?.addRestart()
    board!.showWordInfo()
    removeScoreObserver()
    updateStats(model: global.gameModel)
  }
  
  
  func addScoreObserver() {
    card.statusBar.updateLabel(elementName: "score", updatedValue: global.gameModel.score)
    card.statusBar.updateLabel(elementName: "numFound", updatedValue: global.gameModel.foundWords.count)
    scoreObserver = NotificationCenter.default.addObserver(forName: .didFindWord, object: nil, queue: OperationQueue.main, using: updateScore)
  }
  
  func removeScoreObserver() {
    if (scoreObserver != nil) {
      NotificationCenter.default.removeObserver(scoreObserver!)
    }
  }
  
  func updateScore(Notification: Notification) -> Void {
    print("updating score")
    card.statusBar.updateLabel(elementName: "score", updatedValue: global.gameModel.score)
    card.statusBar.updateLabel(elementName: "numFound", updatedValue: global.gameModel.foundWords.count)
  }

  
  func pauseGame(animated anim: Bool) {
    if (!gamePaused) {
      gamePaused = true
      stopwatch?.pauseView(animated: anim)
      gameTimer?.invalidate()
      foundWordView.disableModelObserver()
      board!.disableBoardInteraction(animated: anim)
      addResetButton()
    }
  }
  
  func unpauseGame(animated anim: Bool) {
    if (gamePaused) {
      gamePaused = false
      stopwatch?.unpauseView(animated: anim)
      startTimer()
      foundWordView.enableModelObserver()
      board!.enableBoardInteraction()
      removeResetButton()
    }
  }
  
  
  private func startTimer() {
    
    if global.gameModel.time < 0 {
      gameTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(infTimer), userInfo: nil, repeats: true)
    } else {
      gameTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(handTimer), userInfo: nil, repeats: true)
    }
  }
  
  
  @objc func infTimer() {
    stopwatch?.updateInf()
  }
  
  
  @objc func handTimer() {
    if global.gameModel.timeRemaining <= 0 && -1 < global.gameModel.timeRemaining  {
      gameOver()
    } else {
      global.gameModel.timeRemaining = global.gameModel.timeRemaining - 0.01
      stopwatch?.updateHand()
    }
  }
  
  private func processTimerInteraction() {
    
    if global.gameModel.timeRemaining != 0 { // If there's a game running
      gamePaused ? unpauseGame(animated: true) : pauseGame(animated: true)
    } else { // if game isn't in progress, a new model needs to be made
      clearOldGame()
      newGame(start: true)
    }
  }
  
  @objc func timerTapped(_ r: UIGestureRecognizer!) {
    processTimerInteraction()
  }
  
  
  func setupControls() {
    timerTapInteraction = UITapGestureRecognizer(target: self, action: #selector(timerTapped(_:)))
    stopwatch?.addGestureRecognizer(timerTapInteraction!)
  }
  
  
  // Update the found words of the model and found words display
  func buildCardBasics() {
    buildFoundWordsTable()
    buildStopwatchView()
  }
  
  // Add a table view to keep track of words found
  func buildFoundWordsTable() {
    foundWordView = FoundWordView(listWidth: cardWidth*0.4625,
                                  listHeight: cardHeight*0.2,
                                  cornerRadius: boardCornerRadius,
                                  shadeWords: false)
    foundWordView.frame.origin.x = cardWidth*0.5125
    foundWordView.frame.origin.y = cardHeight*0.085
    card.addSubview(foundWordView)
  }
  
  // to add the timer
  func buildStopwatchView() {
    
    stopwatch = StopwatchView(position: CGPoint(x: cardWidth*0.025, y: cardHeight*0.085), size: cardHeight*0.2, cornerRadius: boardCornerRadius)
    stopwatch!.addStopwatch()
    
    card.addSubview(stopwatch!)
  }
  
  
  func addResetButton() {
    
    guard (reset == nil) else {
      logw(m: "Attempting to add an extra reset arrow")
      return
    }
    
    let resetSize = cardHeight * 0.2
    let resetYPos = cardHeight - cardWidth + (cardWidth - resetSize)/2
    
    reset = ResetView(position: CGPoint(x: (cardWidth - resetSize)/2, y: resetYPos), size: resetSize)
    card.addSubview(reset!)
    resetTapInteraction = UITapGestureRecognizer(target: self, action: #selector(resetTapped(_:)))
    reset!.addGestureRecognizer(resetTapInteraction!)
  }
  
  func removeResetButton() {
    guard (reset != nil) else {
      logw(m: "Attempting to remove non-existing reset arrow")
      return
    }
    reset?.removeGestureRecognizer(resetTapInteraction!)
    reset?.removeFromSuperview()
    reset = nil
  }
  
  @objc func resetTapped(_ r: UIGestureRecognizer!) {
    gameOver()
  }
  
  override func hideCard() {
    // Probs fine to leave everything as this will be returned to
  }
}
