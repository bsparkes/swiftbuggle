//
//  GameboardViewController.swift
//  buggle
//
//  Created by Benjamin Sparkes on 22/12/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit


class GameboardViewController : UIViewController {
  
  let boardSize : CGFloat
  
  unowned var gameModel: GameModel
  var board: GameboardView?
  
  var rotationObserver : Any? = nil
  
  var wordAttempt: String
  var tileMemory: [TileIndex]
  var currentTile: TileIndex?
  
  
  deinit {
    disableRotationObserver()
    board!.removeFromSuperview()
    
    print("gameboard view controller went away")
  }
  
  
  init(gameModel m: GameModel, boardSize size : CGFloat) {
    
    gameModel = m
    boardSize = size
    
    wordAttempt = ""
    tileMemory = []
    
    super.init(nibName: nil, bundle: nil)
    
    board = GameboardView(boardSize: size, model: m, controller: self)
    
    disableBoardInteraction(animated: false)
  }
  
  
  func addToView(view v: UIView) {
    v.addSubview(board!)
    let buffer = (v.layer.bounds.width - boardSize) / 2
    board!.frame.origin.x = buffer
    board!.frame.origin.y = v.layer.bounds.height - (boardSize + buffer)
  }
  
  func removeFromView(view v: UIView) {
    board!.removeFromSuperview()
  }
  
  
  required init(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func showWordInfo() {
    // Add the two found word views.
    disableRotationObserver()
    board!.hideTiles(animated: false)
    
    let wordListPadding = board!.tilePadding
    let wordListWidth = (boardSize - (wordListPadding * 3)) / 2
    let wordListHeight = boardSize - (wordListPadding * 2)
    
    let gameOverFoundWords = FoundWordView(listWidth: wordListWidth, listHeight: wordListHeight, cornerRadius: getCornerRadius(width: boardSize * 0.95), shadeWords: false, wordList: gameModel.foundWords.sorted { $0.word.count == $1.word.count ? $0.word < $1.word : $0.word.count < $1.word.count })
    gameOverFoundWords.frame.origin.x = wordListPadding
    gameOverFoundWords.frame.origin.y = wordListPadding
    
    let gameOverPossibleWords = FoundWordView(listWidth: wordListWidth, listHeight: wordListHeight, cornerRadius: getCornerRadius(width: boardSize * 0.95), shadeWords: true, wordList: gameModel.possibleWords)
    gameOverPossibleWords.frame.origin.x = (wordListPadding * 2) + wordListWidth
    gameOverPossibleWords.frame.origin.y = wordListPadding
    
    board!.addSubview(gameOverFoundWords)
    board!.addSubview(gameOverPossibleWords)
  }
  
  func disableBoardInteraction(animated: Bool) {
    board!.hideTiles(animated: animated)
    disableRotationObserver()
  }
  
  func enableBoardInteraction() {
    board!.displayTiles(animated: true)
    enableRotationObserver()
  }
  
  func removeSubViews() {
    for subview in board?.subviews ?? [] {
      subview.removeFromSuperview()
    }
  }
  
  func rotateTiles(Notification: Notification) -> Void {
    
   unowned let tempBoard = board! // make sure the notification doesn't make a strong reference
    
    switch UIDevice.current.orientation {
    case .landscapeLeft:
      tempBoard.rotateAllTiles(orientation: "left")
    case .landscapeRight:
      tempBoard.rotateAllTiles(orientation: "right")
    case .portraitUpsideDown:
      tempBoard.rotateAllTiles(orientation: "flip")
    default:
      tempBoard.rotateAllTiles(orientation: "up")
    }
  }
  
  func enableRotationObserver() {
    rotationObserver = NotificationCenter.default.observe(name: UIDevice.orientationDidChangeNotification, object: nil, queue: nil, using: rotateTiles)
  }
  
  func disableRotationObserver(){
    rotationObserver = nil
  }
  
  
  func resetAttemptVariables() {
    wordAttempt = ""
    tileMemory = []
    currentTile = nil
  }
  
  
  private func surroundingTiles(location: TileIndex) -> [TileIndex] {
    let x = location.col
    let y = location.row
    var adjoiningList = [TileIndex]()

    // + 2 because of the less than relation
    for posX in max(0, x - 1) ..< min(x + 2, gameModel.dimension) {
      for posY in max(0, y - 1) ..< min(y + 2, gameModel.dimension) {
        if (posX == x && posY == y) {
        } else {
          adjoiningList.append(TileIndex(row: posY, col: posX))
        }
      }
    }
    return adjoiningList
  }
  
  
  private func isInSurroundingTiles(currentTile: TileIndex, location: TileIndex) -> Bool {
    let cx = currentTile.col
    let cy = currentTile.row
    let lx = location.col
    let ly = location.row
    
    if cx == lx && cy == ly {
      return false
    } else if abs(cx - lx) < 2 && abs(cy - ly) < 2 {
      return true
    } else {
      return false
    }
  }
  
  // Check whether a different tile is being considered, and update if so
  func newTile(selectedTile: TileIndex) -> Bool {
//    print("selectedTile: ")
//    print(selectedTile)
    if currentTile != nil {
//      print("currentTile : ", currentTile!)
    }
//    print("In memory: ", tileMemory.contains(where: {$0 == selectedTile}))
    
    if currentTile == nil {
      currentTile = selectedTile
      return true
//    } else if currentTile! == selectedTile || tileMemory.contains(where: {$0 == selectedTile}) || !surroundingTiles(location: currentTile!).contains(where: {$0 == selectedTile}) {
//      return false
    } else if tileMemory.contains(where: {$0 == selectedTile}) || !isInSurroundingTiles(currentTile: currentTile!, location: selectedTile) {
      return false
    } else {
      currentTile = selectedTile
      return true
    }
  }
  
  
  // Check if the user is backtracking
  func backTracking(selectedTile: TileIndex) -> Bool {
    if tileMemory.count < 2 {
      return false
    } else {
      if selectedTile == tileMemory[tileMemory.count-2] {
        return true
      } else {
        return false
      }
    }
  }
  
  // Update the board when backtracking
  func backtrackTiles() {
    tileMemory.removeLast()
    currentTile = tileMemory.last
  }
  
  // Update the tile history
  func rememberTile(location: TileIndex) {
    let result = tileMemory.filter { $0 == location }
    if result.isEmpty {
      tileMemory.append(location)
    }
  }
  
  // Returns true if a tile is accessible from another tile
  private func tilePath(from: TileIndex, to: TileIndex) -> Bool {
    let adjoiningList = surroundingTiles(location: from).filter { $0 == to}
    if adjoiningList.isEmpty { // ensure that the tile is in the adjoining list
      return false
    } else { // ensure that tile hasn't already been selected
      return tileMemory.filter { $0 == to }.isEmpty
    }
  }
}
