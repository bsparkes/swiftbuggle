//
//  TileFrameView.swift
//  buggle
//
//  Created by Benjamin Sparkes on 05/07/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

class TileView: UIView {
  
  var text: String
  var letterLayers: [CAShapeLayer] = []
  var tileFont: UIFont
  var angle = CGFloat(0)
  let size: CGFloat
  
  let tileFrame: CGRect
  
  required init(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
  deinit {
    //    print("tile deleted")
  }
  
  init(position: CGPoint, width: CGFloat, value: String, cornerRadius cR: CGFloat) {
    
    tileFrame = CGRect(x: position.x, y: position.y, width: width, height: width)
    size = width
    
    let fontSizeAdjust = (getFontPixelSize(fontSize: 12) * size) * 0.5
    
    tileFont = UIFont(name: tileFontName, size: fontSizeAdjust)!
    
    text = value.capitalized
    if (text == "!") {
      text = "Qu"
    }
    
    super.init(frame: tileFrame)
    
    layer.cornerRadius = cR
    
    letterLayers = getStringLayers(text: text, font: tileFont)
    
    var lastWidth = CGFloat(0)
    var maxHeight = CGFloat(0)
    
    for sublayer in letterLayers {
      
      sublayer.position.x = lastWidth
      let indent = sublayer.path!.boundingBox.minX + sublayer.path!.boundingBox.maxX
      lastWidth += CGFloat(indent)
      
      if sublayer.path!.boundingBox.height > maxHeight {
        maxHeight = sublayer.path!.boundingBox.height
      }
      layer.addSublayer(sublayer)
    }
    
    let indent = (width - lastWidth)/2
    
    for sublayer in letterLayers {
      sublayer.position.x += indent
      sublayer.position.y = (width - maxHeight)/2
    }
  }
  
  
  func rotateToOrientation(orientation: String) {
    
    if orientation == "left" {
      tileRotateFromTo(rotation: -angle + CGFloat.pi/2, toAngle: CGFloat.pi/2)
    } else if orientation == "right" {
      tileRotateFromTo(rotation: -angle + -CGFloat.pi/2, toAngle: -CGFloat.pi/2)
    } else if orientation == "flip" {
      tileRotateFromTo(rotation: -angle + CGFloat.pi, toAngle: CGFloat.pi)
    } else {
      tileRotateFromTo(rotation: -angle, toAngle: 0)
    }
  }
  
  
  func tileRotateFromTo(rotation r: CGFloat, toAngle ta: CGFloat) {
    
    var ra = r.truncatingRemainder(dividingBy: .pi*2)
    if (ra < -.pi) {
      ra += .pi*2
    } else if ra > .pi {
      ra -= .pi*2
    }
    
    transform = transform.rotated(by: r)
    
    CATransaction.begin()
    CATransaction.setAnimationDuration(tileAnimDuration)
    let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
    rotation.fromValue = angle
    rotation.byValue = ra
    rotation.duration = 0.5
    rotation.isCumulative = false
    rotation.timingFunction = CAMediaTimingFunction(controlPoints: 0.5, 0.01, 0.5, 1.5)
    CATransaction.setCompletionBlock {
      self.layer.removeAllAnimations()
    }
    CATransaction.commit()
    
    layer.add(rotation, forKey: "rotationAnimation")
    angle = ta
  }
  
  
  func appear(animated: Bool) {
    
    for lay in letterLayers {
      lay.removeAllAnimations()
    }
    
    let borderWidth = size*0.02
    
    if animated {
      let bgcl = CABasicAnimation(keyPath: "backgroundColor")
      bgcl.fromValue = UIColor.clear.cgColor
      bgcl.toValue = tileBackgroundColour.cgColor
      
      CATransaction.begin()
      CATransaction.setAnimationDuration(tileAnimDuration)
      layer.backgroundColor = tileBackgroundColour.cgColor
      layer.borderWidth = borderWidth
      CATransaction.commit()
      
      layer.add(bgcl, forKey: bgcl.keyPath)
      
      for lay in letterLayers {
        animateStringLayerAppear(layer: lay)
        lay.lineWidth = borderWidth
      }
    } else {
      
      layer.backgroundColor = tileBackgroundColour.cgColor
      layer.borderWidth = borderWidth
      
      for lay in letterLayers {
        lay.opacity = 1
        lay.strokeEnd = 1
        lay.lineWidth = borderWidth
        lay.fillColor = tileFillColour.cgColor
      }
    }
  }
  
  
  func disappear(animated: Bool) {
    
    for lay in letterLayers {
      lay.removeAllAnimations()
    }
    
    if animated {
      
      let col = CABasicAnimation(keyPath: "backgroundColor")
      col.fromValue = tileBackgroundColour.cgColor
      col.toValue = UIColor.clear.cgColor
      
      CATransaction.begin()
      CATransaction.setAnimationDuration(tileAnimDuration)
      layer.backgroundColor = UIColor.clear.cgColor
      layer.borderWidth = 0
      CATransaction.commit()
      
      layer.add(col, forKey: col.keyPath)
      
      for lay in letterLayers {
        animateStringLayerDisappear(layer: lay)
      }
    } else {
      
      layer.backgroundColor = UIColor.clear.cgColor
      layer.borderWidth = 0
      
      for lay in letterLayers {
        lay.opacity = 0
        lay.strokeEnd = 0
        lay.fillColor = UIColor.clear.cgColor
      }
    }
  }
  
  
  func tileSelected() {
    for layer in letterLayers {
      layer.fillColor = tileSelectedColour.cgColor
    }
      layer.borderColor = tileSelectedColour.cgColor
  }
  
  func tileDeselected() {
    for layer in letterLayers {
      layer.fillColor = tileFillColour.cgColor
    }
      layer.borderColor = UIColor.black.cgColor
  }
  
}

