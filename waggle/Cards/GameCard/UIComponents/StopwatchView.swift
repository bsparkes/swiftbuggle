//
//  stopwatchView.swift
//  buggle
//
//  Created by Benjamin Sparkes on 01/07/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

class StopwatchView: UIView {
  
  private var stopwatchRunning: Bool = false
  private let size: CGFloat
  private let centerValue: CGFloat
  
  private let radius: CGFloat
  
  private var secondsIncrement: Double
  private var secondsAngle = Double(1.5 * Double.pi)
  private let secondsLength: CGFloat
  
  private var faceLayer = CAShapeLayer()
  private var secondsLayer: CAShapeLayer?
  private var restartArrowOneLayer = CAShapeLayer()
  private var restartArrowTwoLayer = CAShapeLayer()
  private var infLayerOne: CAShapeLayer?
  private var infLayerTwo: CAShapeLayer?
  
  private let lineWidth: CGFloat
  
  init(position: CGPoint, size s: CGFloat, cornerRadius r: CGFloat) {
    
    size = s
    centerValue = s * 0.5
    lineWidth = s*0.05
    radius = s * 0.4
    
    secondsIncrement = (2 * Double.pi) / (Double(global.gameModel.time) + 1)
    secondsLength = radius*0.9 - 2*lineWidth
    
    secondsAngle = secondsAngle + (Double(global.gameModel.time - Int(global.gameModel.timeRemaining)) * secondsIncrement)
    
    
    super.init(frame: CGRect(x: position.x,
                             y: position.y,
                             width: size,
                             height: size))
    
    backgroundColor = UIColor.gray
    layer.cornerRadius = r
  }
  
  
  deinit {
    layer.sublayers?.removeAll()
    print("deleted stopwatch view")
  }
  
  
  private func addFace(yPos: CGFloat) {
    
    let facePath = UIBezierPath(
      arcCenter: CGPoint(x: centerValue, y: yPos),
      radius: CGFloat(radius - (lineWidth * 0.5)),
      startAngle: CGFloat(-(Double.pi / 2)),
      endAngle: CGFloat(3 * (Double.pi / 2)),
      clockwise: true)
    
    faceLayer.path = facePath.cgPath
    faceLayer.fillColor = UIColor.clear.cgColor
    faceLayer.strokeColor = interactiveStrokeColour.cgColor
    faceLayer.lineWidth = lineWidth
    faceLayer.lineDashPattern = [NSNumber(value: (Double.pi * 2 * Double(radius)/6)), NSNumber(value: Double(lineWidth)*2)]
    faceLayer.lineCap = .round
    
    let faceStrokeAnimation = CABasicAnimation(keyPath: "strokeEnd")
    faceStrokeAnimation.fromValue = 0
    faceStrokeAnimation.toValue = 1
    faceStrokeAnimation.duration = defaultAnimationDuration
    faceStrokeAnimation.timingFunction = CAMediaTimingFunction(controlPoints: 0.25, 0, 0.15, 1)
    
    faceLayer.add(faceStrokeAnimation, forKey: faceStrokeAnimation.keyPath)
    
    layer.addSublayer(faceLayer)
  }
  
  
  private func addSecondsHand() {
    
    secondsAngle = Double(1.5 * Double.pi)
    
    let secondsHand = UIBezierPath()
    secondsHand.move(to: CGPoint(x:centerValue, y:centerValue))
    secondsHand.addLine(to: CGPoint(x: centerValue + (secondsLength * CGFloat(cos(secondsAngle))),
                                    y: centerValue + (secondsLength * CGFloat(sin(secondsAngle)))
    ))
    
    secondsLayer = CAShapeLayer()
    secondsLayer!.lineCap = .round
    secondsLayer!.path = secondsHand.cgPath
    secondsLayer!.strokeColor = interactiveStrokeColour.cgColor
    secondsLayer!.lineWidth = lineWidth
    
    layer.addSublayer(secondsLayer!)
  }
  
  
  private func addInf() {
    
    let infLayerA = makeInfLayer(size: size, lineWidth: lineWidth, scale: 0.8, colour: interactiveStrokeColour.cgColor, gap: false)
    let infLayerB = makeInfLayer(size: size, lineWidth: lineWidth, scale: 0.8, colour: interactiveStrokeColour.cgColor, gap: false)
    
    infLayerOne = infLayerA
    infLayerTwo = infLayerB
    
    layer.addSublayer(infLayerOne!)
    infLayerOne!.strokeStart = 0.1
    infLayerOne!.strokeEnd = 1
    layer.addSublayer(infLayerTwo!)
    infLayerTwo!.strokeStart = 0
    infLayerTwo!.strokeEnd = 0.05
  }
  
  
  private func removeInf() {
    
    guard (infLayerOne != nil && infLayerTwo != nil) else { return }
    
    infLayerOne!.removeFromSuperlayer()
    infLayerOne = nil
    infLayerTwo!.removeFromSuperlayer()
    infLayerTwo = nil
  }
  
  public func updateInf() {
    // Animate the icon, need to remove animation every time.
    guard (infLayerOne != nil && infLayerTwo != nil) else {
      return }
    
    let startVal = (infLayerOne!.strokeEnd + 0.00025).truncatingRemainder(dividingBy: 1)
    let endVal = (startVal + 0.1).truncatingRemainder(dividingBy: 1)
    
    if (endVal > startVal) {
      infLayerOne!.strokeStart = 0
      infLayerOne!.strokeEnd = startVal
      infLayerTwo!.strokeStart = endVal
      infLayerTwo!.strokeEnd = 1
    } else {
      infLayerOne!.strokeStart = endVal
      infLayerOne!.strokeEnd = startVal
      infLayerTwo!.strokeStart = 0
      infLayerTwo!.strokeEnd = 0
    }
    infLayerOne!.removeAllAnimations()
    infLayerTwo!.removeAllAnimations()
  }
  
  
  private func removeHandOrInf() {
    if (secondsLayer != nil) {
      secondsLayer!.removeFromSuperlayer()
      secondsLayer = nil
    }
    if (infLayerOne != nil && infLayerTwo != nil) {
      infLayerOne!.removeFromSuperlayer()
      infLayerTwo!.removeFromSuperlayer()
      infLayerOne = nil
      infLayerTwo = nil
    }
  }
  
  
  func addStopwatch() {
    addFace(yPos: centerValue)
    displayHandOrInf()
    
    pauseView(animated: false)
  }
  
  func addRestart() {
    unpauseView(animated: false)
    addRestartArrows(yPos: centerValue)
  }
  
  func removeRestart() {
    pauseView(animated: false)
    restartArrowOneLayer.removeFromSuperlayer()
    restartArrowTwoLayer.removeFromSuperlayer()
  }
  
  
  private func addRestartArrows(yPos: CGFloat) {
    
    removeHandOrInf()
    
    for lay in layer.sublayers! { lay.removeAllAnimations() }
    
    let arrowColour = UIColor.darkGray.cgColor
    
    restartArrowOneLayer.path =  resetArrowPath(yPos: yPos).cgPath
    restartArrowOneLayer.fillColor = UIColor.clear.cgColor
    restartArrowOneLayer.strokeColor = arrowColour
    restartArrowOneLayer.lineWidth = lineWidth
    
    let restartTwoArrow = resetArrowPath(yPos: yPos)
    
    restartTwoArrow.apply(CGAffineTransform(translationX: centerValue, y: yPos).inverted())
    restartTwoArrow.apply(CGAffineTransform(rotationAngle: CGFloat(Double.pi)))
    restartTwoArrow.apply(CGAffineTransform(translationX: centerValue, y: yPos))
    
    restartArrowTwoLayer.fillColor = UIColor.clear.cgColor
    restartArrowTwoLayer.strokeColor = arrowColour
    restartArrowTwoLayer.lineWidth = lineWidth
    
    restartArrowTwoLayer.path =  restartTwoArrow.cgPath
    
    let strokeAnimation = CABasicAnimation(keyPath: "strokeEnd")
    strokeAnimation.fromValue = 0.0
    strokeAnimation.toValue = 1.0
    strokeAnimation.duration = defaultAnimationDuration
    strokeAnimation.timingFunction = CAMediaTimingFunction(controlPoints: 0, 0, 0.5, 1)
    
    let colourAnimation = CABasicAnimation(keyPath: "opacity")
    colourAnimation.fromValue = 0.5
    colourAnimation.toValue = 1
    colourAnimation.duration = defaultAnimationDuration
    colourAnimation.timingFunction = CAMediaTimingFunction(controlPoints: 0.5, 0, 0.5, 1)
    
    restartArrowOneLayer.add(strokeAnimation, forKey: strokeAnimation.keyPath)
    restartArrowOneLayer.add(colourAnimation, forKey: colourAnimation.keyPath)
    restartArrowTwoLayer.add(strokeAnimation, forKey: strokeAnimation.keyPath)
    restartArrowTwoLayer.add(colourAnimation, forKey: colourAnimation.keyPath)
    
    layer.sublayers?.insert(restartArrowOneLayer, at: 0)
    layer.sublayers?.insert(restartArrowTwoLayer, at: 0)
  }
  
  
  private func resetArrowPath(yPos: CGFloat) -> UIBezierPath {
    
    let restartInset = lineWidth * 3
    let resetRadius = radius - restartInset
    let resetEndAngle = CGFloat.pi*0.8
    
    let miniRadius = restartInset
    
    let circleTwoPoint = CGPoint(x: centerValue + ((radius - restartInset) * CGFloat(cos(resetEndAngle))),
                                 y: yPos + ((radius - restartInset) * CGFloat(sin(resetEndAngle))))
    
    let intersectionPoints = getIntersection(circleOne: CGPoint(x: centerValue, y: centerValue), radiusOne: resetRadius, circleTwo: circleTwoPoint, radiusTwo: miniRadius)
    
    let test = intersectionPoints[0]
    
    let intersectionAngle = atan2(test.y - circleTwoPoint.y, test.x - circleTwoPoint.x)
    
    let arrowCord = 2*lineWidth
    let arrowStartAngle = intersectionAngle - CGFloat.pi/8
    let arrowEndAngle = intersectionAngle + CGFloat.pi/8
    
    let arrowMeetPoint = CGPoint(x: centerValue + ((radius - restartInset) * CGFloat(cos(resetEndAngle))),
                                 y: yPos + ((radius - restartInset) * CGFloat(sin(resetEndAngle))))
    
    let arrowFirstPoint = CGPoint(x: arrowMeetPoint.x + arrowCord * CGFloat(cos(arrowStartAngle)),
                                  y: arrowMeetPoint.y + arrowCord * CGFloat(sin(arrowStartAngle)))
    
    let arrowSecondPoint = CGPoint(x: arrowMeetPoint.x + arrowCord * CGFloat(cos(arrowEndAngle)),
                                   y: arrowMeetPoint.y + arrowCord * CGFloat(sin(arrowEndAngle)))
    
    let arrowBasePoint = CGPoint(x: centerValue + ((radius - restartInset) * CGFloat(cos(resetEndAngle))),
                                 y: yPos + ((radius - restartInset) * CGFloat(sin(resetEndAngle))))
    
    let restartLine = UIBezierPath(
      arcCenter: CGPoint(x: centerValue, y: yPos),
      radius: CGFloat(radius - restartInset),
      startAngle: CGFloat(0),
      endAngle:CGFloat(resetEndAngle),
      clockwise: true)
    
    let restartArrow = UIBezierPath()
    
    restartArrow.move(to: arrowFirstPoint )
    restartArrow.addLine(to: arrowMeetPoint)
    restartArrow.addLine(to: arrowSecondPoint)
    restartArrow.addArc(withCenter: arrowBasePoint, radius: arrowCord, startAngle: arrowEndAngle, endAngle: arrowStartAngle, clockwise: false)
    restartArrow.close()
    
    restartLine.append(restartArrow)
    
    return restartLine
  }
  
  
  public func tapToggleStopwatch(animated anim: Bool) {
    
    if stopwatchRunning { // move up
      pauseView(animated: anim)
    } else { // move down
      unpauseView(animated: anim)
    }
    stopwatchRunning.toggle()
  }
  
  
  public func unpauseView(animated anim: Bool) {
    faceLayer.lineDashPattern = [NSNumber(value: (Double.pi * 2 * Double(radius)/6)), 0]
  }
  
  
  public func pauseView(animated anim: Bool) {
    
    let numberOfLines = 7
    let percent = 0.65
    let dash = (2*Double.pi*Double(radius - (lineWidth * 0.5))*percent)/Double(numberOfLines)
    let space = (2*Double.pi*Double(radius - (lineWidth * 0.5))*(1 - percent))/Double(numberOfLines)
    
    let NSDash = NSNumber(value: dash)
    let NSSpace = NSNumber(value: space)
    faceLayer.lineDashPattern = [NSDash, NSSpace]
  }
  
  public func setHandPosition() {
    
    let secondsPassed = modf(Double(global.gameModel.time)).0 - modf(global.gameModel.timeRemaining).0
    print("Seconds passed: \(secondsPassed)")
    moveHand(ticks: secondsPassed)
  }
  
  public func updateHand() {
    
    let t = global.gameModel.timeRemaining.truncatingRemainder(dividingBy: 1)
    if (0.44 < t && t < 0.45) {
      moveHand(ticks: 1)
    }
  }
  
  private func moveHand(ticks tk: Double) {
    guard (secondsLayer != nil) else { return }
    secondsAngle = secondsAngle + secondsIncrement*tk
    let secondsHand = UIBezierPath()
    secondsHand.move(to: CGPoint(x: centerValue, y: centerValue))
    secondsHand.addLine(to: CGPoint(x: centerValue + (secondsLength * CGFloat(cos(secondsAngle))),
                                    y: centerValue + (secondsLength * CGFloat(sin(secondsAngle)))
    ))
    secondsLayer!.lineCap = .round
    secondsLayer!.lineJoin = .round
    secondsLayer!.path = secondsHand.cgPath
  }
  
  func resetTimer() {
    removeRestart()
    displayHandOrInf()
  }
  
  
  func displayHandOrInf() {
    if (global.gameModel.time > 0) {
      secondsIncrement = (2 * Double.pi) / (Double(global.gameModel.time) + 1)
      if (secondsLayer == nil) {
        removeInf()
        addSecondsHand()
      }
    } else if (global.gameModel.time < 0 && infLayerOne == nil) {
      removeHandOrInf()
      addInf()
    }
  }
  
  required init(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
}
