//
//  ResetView.swift
//  buggle
//
//  Created by Benjamin Sparkes on 13/05/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import UIKit

class ResetView : UIView {
  
  private let centerPoint: CGPoint
  private let lineWidth: CGFloat
  private let radius: CGFloat
  
  private var size: CGFloat
  private var faceLayer = CAShapeLayer()
  
  init(position: CGPoint, size s: CGFloat) {
    
    size = s
    centerPoint = CGPoint(x: s/2, y: s/2)
    lineWidth = CGFloat(s*0.05)
    radius = s * 0.45
    
      super.init(frame: CGRect(x: position.x,
                               y: position.y,
                               width: size,
                               height: size))
    
    backgroundColor = UIColor.clear
    addFace()
    addTick()
  }
  
  
  deinit {
    layer.sublayers?.removeAll()
    print("deinit reset view")
  }
  
  private func addFace() {
    
    let facePath = UIBezierPath(
      arcCenter: centerPoint,
      radius: CGFloat(radius - (lineWidth * 0.5)),
      startAngle: CGFloat(-(Double.pi / 2)),
      endAngle: CGFloat(3 * (Double.pi / 2)),
      clockwise: true)
    
    let faceLayer = CAShapeLayer()
    
    faceLayer.path = facePath.cgPath
    faceLayer.fillColor = UIColor.clear.cgColor
    faceLayer.strokeColor = UIColor.black.cgColor
    faceLayer.lineWidth = lineWidth
    
//    let faceStrokeAnimation = CABasicAnimation(keyPath: "strokeEnd")
//    faceStrokeAnimation.fromValue = 0
//    faceStrokeAnimation.toValue = 1
//    faceStrokeAnimation.duration = defaultAnimationDuration
//    faceStrokeAnimation.timingFunction = CAMediaTimingFunction(controlPoints: 0.25, 0, 0.15, 1)
//
//    faceLayer.add(faceStrokeAnimation, forKey: faceStrokeAnimation.keyPath)
    
    layer.addSublayer(faceLayer)
  }
  
  func addTick() {
    
    let tickRadius = radius*0.6
    
    let tickOrigin = getPoint(center: centerPoint, radius: tickRadius, angle: 3*CGFloat.pi/4)
    let tickTopEnd = getPoint(center: centerPoint, radius: tickRadius, angle: CGFloat.pi/4)
    let tickWidth = tickTopEnd.x - tickOrigin.x
    let tickBasePont = CGPoint(x: tickOrigin.x + tickWidth * 1/3, y: tickOrigin.y + tickWidth)
    let tickLeftPoint = CGPoint(x: tickOrigin.x, y: tickOrigin.y + tickWidth * 2/3)
    
    let test = UIBezierPath()
    test.move(to: tickTopEnd)
    test.addLine(to: tickBasePont)
    test.addLine(to: tickLeftPoint)
    
    let testLayer = CAShapeLayer()
    testLayer.path = test.cgPath
    testLayer.strokeColor = UIColor.black.cgColor
    testLayer.fillColor = UIColor.clear.cgColor
    testLayer.lineCap = .round
    testLayer.lineJoin = .round
    testLayer.lineWidth = lineWidth
    
    layer.addSublayer(testLayer)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
