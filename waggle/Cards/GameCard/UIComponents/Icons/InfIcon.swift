//
//  InfIcon.swift
//  buggle
//
//  Created by Benjamin Sparkes on 21/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import UIKit


func getPoint(center c: CGPoint, radius r: CGFloat, angle a: CGFloat) -> CGPoint {
  let xPos = c.x + r*cos(a)
  let yPos = c.y - r*sin(a)
  return CGPoint(x: xPos, y: yPos)
}

func findTangent(point p: CGPoint, circleCenter cc: CGPoint, radius r:CGFloat, clockwise: Bool) -> CGFloat {
  
  let d =  sqrt(pow(p.x - cc.x, 2) + pow(p.y - cc.y, 2))
  let rd = r/d
  let theta = acos(rd)
  let delta = atan2(p.y - cc.y, p.x - cc.x)
  
  if clockwise {
    return delta - theta
  } else {
    return delta + theta
  }
}


class infIcon : IconView {
  
  let lineColour = UIColor.darkGray.cgColor
  
  override init(size s: CGFloat) {
    
    super.init(size: s)
    
    backgroundColor = UIColor.clear
    
    let infLayer = makeInfLayer(size: s, lineWidth: s*0.1, scale: 1, colour: lineColour, gap: true)
    layer.addSublayer(infLayer)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}


func makeInfLayer(size s: CGFloat, lineWidth lw: CGFloat, scale sc: CGFloat, colour c: CGColor, gap: Bool) -> CAShapeLayer {
  
  var layers = [CAShapeLayer]()
  
  let lineWidth = lw
  let centerPoint = CGPoint(x: s/2, y: s/2)
  let circRadius = s*0.15 * sc
  let spacing = s*0.4 * sc
  
  let leftCenterPoint = CGPoint(x: (s - spacing)/2, y: s/2)
  let rightCenterPoint = CGPoint(x: (s + spacing)/2, y: s/2)
  
  let leftTangent = findTangent(point: centerPoint, circleCenter: leftCenterPoint, radius: circRadius, clockwise: false)
  
  let rightTangent = findTangent(point: centerPoint, circleCenter: rightCenterPoint, radius: circRadius, clockwise: true)
  
  let leftCircle = UIBezierPath(arcCenter: leftCenterPoint, radius: circRadius, startAngle: -leftTangent, endAngle: leftTangent, clockwise: false)
  
  let joiningLine = UIBezierPath()
  joiningLine.move(to: CGPoint(x: leftCenterPoint.x + circRadius*cos(leftTangent), y: leftCenterPoint.y + circRadius*sin(leftTangent)))
  joiningLine.addLine(to: CGPoint(x: rightCenterPoint.x + circRadius*cos(rightTangent), y: rightCenterPoint.y - circRadius*sin(rightTangent)))
  
  let rightCircle = UIBezierPath(arcCenter: rightCenterPoint, radius: circRadius, startAngle: -rightTangent, endAngle: rightTangent, clockwise: true)
  
  let infPath = UIBezierPath()
  infPath.append(leftCircle)
  infPath.append(joiningLine)
  infPath.append(rightCircle)
  
  if (!gap) {
    
    let flippedJoin = UIBezierPath()
    let leftTangent = findTangent(point: centerPoint, circleCenter: leftCenterPoint, radius: circRadius, clockwise: true)
    
    let rightTangent = findTangent(point: centerPoint, circleCenter: rightCenterPoint, radius: circRadius, clockwise: false)
    flippedJoin.move(to: CGPoint(x: rightCenterPoint.x + circRadius*cos(rightTangent), y: rightCenterPoint.y - circRadius*sin(rightTangent)))
    flippedJoin.addLine(to: CGPoint(x: leftCenterPoint.x + circRadius*cos(leftTangent), y: leftCenterPoint.y + circRadius*sin(leftTangent)))
    
    infPath.append(flippedJoin)
  }
  
  let infLayer = CAShapeLayer()
  infLayer.path = infPath.cgPath
  infLayer.lineWidth = lineWidth
  infLayer.strokeColor = c
  infLayer.fillColor = UIColor.clear.cgColor
  infLayer.lineCap = .round
  
  layers.append(infLayer)
  
  return infLayer
}
