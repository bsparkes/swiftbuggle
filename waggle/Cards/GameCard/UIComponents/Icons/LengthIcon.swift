//
//  LengthIcon.swift
//  buggle
//
//  Created by Benjamin Sparkes on 18/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import UIKit


class LengthIcon: IconView {
  
  let baseColour = UIColor.lightGray.cgColor
  let notchColour = UIColor.darkGray.cgColor
  
  let baseWidth: CGFloat
  let notchWidth: CGFloat
  
  var firstDot: CAShapeLayer?
  var secondDot: CAShapeLayer?
  var thirdDot: CAShapeLayer?
  
  override init(size s: CGFloat) {
    
    baseWidth = CGFloat(s)*0.025
    notchWidth = CGFloat(s)*0.075
    
    super.init(size: s)
    
    addRuler()
    updateIcon(value: global.gameConfig.getCurrentOption(option: global.gameConfig.lengthID, new: false))
  }
  
  
  override func updateIcon(value v: Int) {
    
    switch v {
    case 3:
      if firstDot == nil {
        addFirstDot()
      }
      removeSecondDot()
      removeThirdDot()
    case 4:
      if firstDot == nil {
        addFirstDot()
      }
      if secondDot == nil {
        addSecondDot()
      }
      removeThirdDot()
    case 5:
      if firstDot == nil {
        addFirstDot()
      }
      if secondDot == nil {
        addSecondDot()
      }
      if thirdDot == nil {
        addThirdDot()
      }
    default:
      removeFirstDot()
      removeSecondDot()
      removeThirdDot()
    }
  }
  
  func addRuler() {
    
    let base = UIBezierPath()
    
    base.move(to: CGPoint(x: size*0.25, y: size*0.95))
    base.addLine(to: CGPoint(x: size*0.95, y: size*0.25))
    base.addLine(to: CGPoint(x: size*0.75, y: size*0.05))
    base.addLine(to: CGPoint(x: size*0.05, y: size*0.75))
    base.addLine(to: CGPoint(x: size*0.25, y: size*0.95))
    
    let baseLayer = CAShapeLayer()
    baseLayer.path = base.cgPath
    
    baseLayer.fillColor = baseColour
    baseLayer.strokeColor = baseColour
    baseLayer.lineWidth = baseWidth
    baseLayer.lineCap = .round
    baseLayer.lineJoin = .round
    baseLayer.cornerRadius = baseWidth
    
    layer.addSublayer(baseLayer)
    
    let firstMaskLayer = CAShapeLayer()
    firstMaskLayer.path = base.cgPath
    
    firstMaskLayer.fillColor = baseColour
    firstMaskLayer.strokeColor = baseColour
    firstMaskLayer.lineWidth = baseWidth
    firstMaskLayer.lineCap = .round
    firstMaskLayer.lineJoin = .round
    firstMaskLayer.cornerRadius = baseWidth
    
    
    let firstNotch = UIBezierPath()
    firstNotch.move(to: CGPoint(x: size*0.225, y: size*0.575))
    firstNotch.addLine(to: CGPoint(x: size*0.3125, y: size*0.6625))
    
    
    let firstNotchLayer = CAShapeLayer()
    firstNotchLayer.path = firstNotch.cgPath
    firstNotchLayer.fillColor = notchColour
    firstNotchLayer.strokeColor = notchColour
    firstNotchLayer.lineCap = .round
    firstNotchLayer.lineWidth = notchWidth
    firstNotchLayer.mask = firstMaskLayer
    
    layer.addSublayer(firstNotchLayer)
    
    
    let secondNotch = UIBezierPath()
    secondNotch.move(to: CGPoint(x: size*0.4, y: size*0.4))
    secondNotch.addLine(to: CGPoint(x: size*0.4875, y: size*0.4875))
    
    let secondMaskLayer = CAShapeLayer()
    secondMaskLayer.path = base.cgPath
    
    secondMaskLayer.fillColor = baseColour
    secondMaskLayer.strokeColor = baseColour
    secondMaskLayer.lineWidth = baseWidth
    secondMaskLayer.lineCap = .round
    secondMaskLayer.lineJoin = .round
    secondMaskLayer.cornerRadius = baseWidth
    
    let secondNotchLayer = CAShapeLayer()
    secondNotchLayer.path = secondNotch.cgPath
    secondNotchLayer.fillColor = notchColour
    secondNotchLayer.strokeColor = notchColour
    secondNotchLayer.lineCap = .round
    secondNotchLayer.lineWidth = notchWidth
    secondNotchLayer.mask = secondMaskLayer
    
    layer.addSublayer(secondNotchLayer)
    
    
    let thirdNotch = UIBezierPath()
    thirdNotch.move(to: CGPoint(x: size*0.575, y: size*0.225))
    thirdNotch.addLine(to: CGPoint(x: size*0.6625, y: size*0.3125))
    
    let thirdMaskLayer = CAShapeLayer()
    thirdMaskLayer.path = base.cgPath
    
    thirdMaskLayer.fillColor = baseColour
    thirdMaskLayer.strokeColor = baseColour
    thirdMaskLayer.lineWidth = baseWidth
    thirdMaskLayer.lineCap = .round
    thirdMaskLayer.lineJoin = .round
    thirdMaskLayer.cornerRadius = baseWidth
    
    
    let thirdNotchLayer = CAShapeLayer()
    thirdNotchLayer.path = thirdNotch.cgPath
    thirdNotchLayer.fillColor = notchColour
    thirdNotchLayer.strokeColor = notchColour
    thirdNotchLayer.lineCap = .round
    thirdNotchLayer.lineWidth = notchWidth
    thirdNotchLayer.mask = thirdMaskLayer
    
    layer.addSublayer(thirdNotchLayer)
  }
  
  func addFirstDot() {
    let firstDotPoint = CGPoint(x: size*0.1375, y: size*0.4875)
    
    let firstDotPath = UIBezierPath(arcCenter: firstDotPoint, radius: notchWidth, startAngle: 0, endAngle: CGFloat.pi*2, clockwise: true)
    
    let firstDotLayer = CAShapeLayer()
    firstDotLayer.path = firstDotPath.cgPath
    firstDotLayer.fillColor = UIColor.lightGray.cgColor
    
    firstDot = firstDotLayer
    
    layer.addSublayer(firstDotLayer)
  }
  
  func removeFirstDot() {
    if firstDot != nil {
      firstDot?.removeFromSuperlayer()
      firstDot = nil
    }
  }
  
  func addSecondDot() {
    let secondDotPoint = CGPoint(x: size*0.3125, y: size*0.3125)
    
    let secondDotPath = UIBezierPath(arcCenter: secondDotPoint, radius: notchWidth, startAngle: 0, endAngle: CGFloat.pi*2, clockwise: true)
    
    let secondDotLayer = CAShapeLayer()
    secondDotLayer.path = secondDotPath.cgPath
    secondDotLayer.fillColor = UIColor.lightGray.cgColor
    
    secondDot = secondDotLayer
    
    layer.addSublayer(secondDotLayer)
  }
  
  func removeSecondDot() {
    if secondDot != nil {
      secondDot?.removeFromSuperlayer()
      secondDot = nil
    }
  }
  
  
  func addThirdDot() {
    let thirdDotPoint = CGPoint(x: size*0.4875, y: size*0.1375)
    
    let thirdDotPath = UIBezierPath(arcCenter: thirdDotPoint, radius: notchWidth, startAngle: 0, endAngle: CGFloat.pi*2, clockwise: true)
    
    let thirdDotLayer = CAShapeLayer()
    thirdDotLayer.path = thirdDotPath.cgPath
    thirdDotLayer.fillColor = UIColor.lightGray.cgColor
    
    thirdDot = thirdDotLayer
    
    layer.addSublayer(thirdDotLayer)
  }
  
  func removeThirdDot() {
    if thirdDot != nil {
      thirdDot?.removeFromSuperlayer()
      thirdDot = nil
    }
  }
  
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
