//
//  lookingglassView.swift
//  buggle
//
//  Created by Benjamin Sparkes on 15/07/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

class LookingGlassView: UIView {
  
  var size: CGFloat
  let centerX: CGFloat
  let centerY: CGFloat
  let radius: CGFloat
  let glassLineWidth: CGFloat = 3
  
  
  var handleLayer = CAShapeLayer()
  var stemLayer = CAShapeLayer()
  var lensLayer = CAShapeLayer()
  let border: CGFloat = 3
  
  init(position: CGPoint, size s: CGFloat) {
    size = s
    radius = size*0.375
    centerX = size/2
    centerY = size/2

      super.init(frame: CGRect(x: position.x,
                               y: position.y,
                               width: size,
                               height: size))
    layer.backgroundColor = UIColor.clear.cgColor
    addLookingGlass()
  }
  
  func addLookingGlass() {

    let radius = frame.width*0.25
    let stemLength = radius*0.25
    let handleLength = (frame.width)/2
    let lensDiameter = frame.width*0.5
    
    let boundingDiagonalLength = sqrt((2*(size*size)))
    let freeSpace = boundingDiagonalLength - ((stemLength + glassLineWidth) + (handleLength + glassLineWidth) + (lensDiameter + glassLineWidth))
    let move = sqrt((freeSpace*freeSpace)/2)
    
    let circleEdge = CGPoint(x: ((radius + glassLineWidth)*CGFloat(cos(Double.pi*0.75)) + frame.width*0.75 - move/4), y: ((radius + glassLineWidth)*CGFloat(sin(Double.pi*0.75)) + frame.width*0.25 + move/4))
    
    // handle drawing
    let handlePathOrigin = CGPoint(x: 0, y: glassLineWidth)
    let handlePath = UIBezierPath(rect: CGRect(x: handlePathOrigin.x+stemLength, y: handlePathOrigin.y-glassLineWidth, width: handleLength, height: glassLineWidth*2))
    handlePath.apply(CGAffineTransform(translationX: handlePathOrigin.x, y: handlePathOrigin.y).inverted())
    handlePath.apply(CGAffineTransform(rotationAngle: CGFloat(3*Double.pi/4)))
    handlePath.apply(CGAffineTransform(translationX: handlePathOrigin.x, y: handlePathOrigin.y))
    
    handlePath.lineJoinStyle = .bevel
    handleLayer.path = handlePath.cgPath
    handleLayer.fillColor = UIColor.clear.cgColor
    handleLayer.strokeColor = interactiveStrokeColour.cgColor
    handleLayer.lineWidth = glassLineWidth
    
    handleLayer.frame.origin = circleEdge
    
    // lens drawing
    let lensPath = UIBezierPath(ovalIn: CGRect(x: (frame.width-border)/2 - move/4, y: border + move/4, width: frame.width/2, height: frame.width/2))
    lensPath.lineJoinStyle = .bevel
    lensLayer.path = lensPath.cgPath
    lensLayer.fillColor = UIColor.clear.cgColor
    lensLayer.strokeColor = interactiveStrokeColour.cgColor
    lensLayer.lineWidth = glassLineWidth
    
    
    // stem drawing
    let stemPathOrigin = CGPoint(x: 0, y: (glassLineWidth - 1))
    let stemPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: stemLength, height: stemLength/3))
    stemPath.apply(CGAffineTransform(translationX: stemPathOrigin.x, y: stemPathOrigin.y).inverted())
    stemPath.apply(CGAffineTransform(rotationAngle: CGFloat(3*Double.pi/4)))
    stemPath.apply(CGAffineTransform(translationX: stemPathOrigin.x, y: stemPathOrigin.y))
    
    stemPath.lineJoinStyle = .bevel
    stemLayer.path = stemPath.cgPath
    stemLayer.fillColor = UIColor.clear.cgColor
    stemLayer.strokeColor = interactiveStrokeColour.cgColor
    stemLayer.lineWidth = (glassLineWidth - 1)
    stemLayer.frame.origin = CGPoint(x: circleEdge.x, y: circleEdge.y)
    
    layer.addSublayer(handleLayer)
    layer.addSublayer(stemLayer)
    layer.addSublayer(lensLayer)
  }
  
  func removeLookingGlass() {
    
    // need to complete.
    
  }
  
  
  required init(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
}

