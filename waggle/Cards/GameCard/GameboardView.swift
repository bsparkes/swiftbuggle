//
//  GameboardView.swift
//  buggle
//
//  Created by Benjamin Sparkes on 19/06/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//


import UIKit


class GameboardView : UIView {
  
  let boardSize : CGFloat
  let tileWidth: CGFloat
  let tilePadding: CGFloat
  
  let tilePanGestureRecogniser = UIPanGestureRecognizer()
  
  unowned var gameModel: GameModel?
  unowned var controller: GameboardViewController?
  
  var tiles = Dictionary<TileIndex, TileView>()
  
  init(boardSize size: CGFloat, model m: GameModel, controller c: GameboardViewController) {
    
    boardSize = size
    gameModel = m
    controller = c
    
    let unpaddedSize = boardSize / CGFloat(gameModel!.dimension)
    tileWidth = unpaddedSize * 0.9
    let tileWidthSum = tileWidth * CGFloat(gameModel!.dimension)
    tilePadding = (boardSize - tileWidthSum) / CGFloat(gameModel!.dimension + 1)
    
    super.init(frame: CGRect(x: 0, y: 0, width: size, height: size))
    
    buildTiles(abstractTiles: gameModel!.abstractGameBoard)
    tilePanGestureRecogniser.addTarget(self, action: #selector(tilePanned(_:)))
    
    layer.cornerRadius = getCornerRadius(width: size)
    backgroundColor = boardBackgroundColour
  }
  
  init(boardSize size : CGFloat, abstractTiles t: Dictionary<TileIndex, String>) {
    
    let dimensionFloat = CGFloat(t.count).squareRoot() // Used for all boards, so can't be replaced with call to gameModel
    
    boardSize = size
    
    let unpaddedSize = boardSize / dimensionFloat
    tileWidth = unpaddedSize * 0.9
    let tileWidthSum = tileWidth * dimensionFloat
    tilePadding = (boardSize - tileWidthSum) / (dimensionFloat + 1)
    
    gameModel = nil
    controller = nil
    
    super.init(frame: CGRect(x: 0, y: 0, width: size, height: size))
    
    layer.cornerRadius = getCornerRadius(width: size)
    backgroundColor = boardBackgroundColour
    
    buildTiles(abstractTiles: t)
    displayTiles(animated: false)
    
  }
  
  deinit {
    for view in subviews {
      view.removeFromSuperview()
    }
    print("game board view went away")
  }
  
  required init(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
  
  func createTileView(at pos: TileIndex, value v: String) {
    let col = tilePadding + CGFloat(pos.col)*(tileWidth + tilePadding)
    let row = tilePadding + CGFloat(pos.row)*(tileWidth + tilePadding)
    let tile = TileView(position: CGPoint(x: col, y: row), width: tileWidth, value: v, cornerRadius: getCornerRadius(width: boardSize))
    
    addSubview(tile)
    bringSubviewToFront(tile)
    
    tiles[pos] = tile
  }
  
  func buildTiles(abstractTiles t: Dictionary<TileIndex, String>) {
    for (key, value) in t {
      createTileView(at: key, value: String(value))
    }
  }
  
  
  func displayTiles(animated anim: Bool) {
    for (_, tile) in tiles {
      tile.appear(animated: anim)
    }
    addGestureRecognizer(tilePanGestureRecogniser)
  }
  
  
  func hideTiles(animated: Bool) {
    for (_, tile) in tiles {
      tile.disappear(animated: animated)
    }
    removeGestureRecognizer(tilePanGestureRecogniser)
  }
}


extension GameboardView { // user interaction
  
  // Returns a tile when the x, y position is in the middle half.
  func getTileFromThumb(x: CGFloat, y: CGFloat) -> TileIndex? {
    
    var xP = x/boardSize // Take the x/y as a percent of how far across the board
    var yP = y/boardSize
    
    guard 0 < yP && yP < 1 && 0 < xP && xP < 1 else {
      return nil
    }
    
    let divFac = 1 / CGFloat(gameModel!.dimension) // Increase the integer component to respresent the square we'd be on
    yP = yP / divFac
    xP = xP / divFac
    
    let yT = yP.truncatingRemainder(dividingBy: 1) // Capture the decimal component, a percentage of where we are in the square
    let xT = xP.truncatingRemainder(dividingBy: 1)
    
    guard 0.1 < yT && yT < 0.9 && 0.1 < xT && xT < 0.9 else { // Ignore touches that only go to the edge of a tile
      return nil
    }
    return TileIndex(row : Int(yP), col: Int(xP)) // If so, return the integer part
  }
  
  
  @objc func tilePanned(_ r: UIGestureRecognizer!) {
    
    let tileFeedback = UIImpactFeedbackGenerator(style: .light)
    
    // Specify where the coordinates are coming from…
    let viewLocation = r.location(in: self)
    // Get the tile, if it's selected…
    if let thumbTileIndex = getTileFromThumb(x: viewLocation.x, y: viewLocation.y) {
      if controller!.newTile(selectedTile: thumbTileIndex) {
        
        tileFeedback.impactOccurred()
        
        controller!.rememberTile(location: thumbTileIndex)
        tiles[thumbTileIndex]?.tileSelected()
        
      } else if controller!.backTracking(selectedTile: thumbTileIndex) {
        
        tileFeedback.impactOccurred()
        
        tiles[TileIndex(row: (controller!.tileMemory.last?.row)!, col: (controller!.tileMemory.last?.col)!)]?.tileDeselected()
        controller!.backtrackTiles()
      }
    }
    
    // When lifting the thumb…
    if r.state == UIGestureRecognizer.State.ended {
      let currentWord = gameModel!.tilelistToWord(tileList: controller!.tileMemory)
      gameModel!.processWord(word: currentWord)
      // deselect tiles in memory
      for tile in (controller!.tileMemory) {
        tiles[tile]?.tileDeselected()
      }
      controller!.resetAttemptVariables()
    }
  }
}


extension GameboardView { // rotation
  func rotateAllTiles(orientation o: String) {
    for (_, tile) in tiles {
      tile.rotateToOrientation(orientation: o)
    }
  }
  
}



