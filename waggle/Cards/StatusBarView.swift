//
//  cardStatusBar.swift
//  buggle
//
//  Created by Benjamin Sparkes on 16/12/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

class StatusBarUIView: UIView, UIGestureRecognizerDelegate {
  
  let statusBarWidth: CGFloat
  let statusBarHeight: CGFloat
  var elementsDictionary = [String: StatusBarElement]()
  var elementsOrder = [String]()
  
  init(cardWidth width: CGFloat, cardHeight height: CGFloat) {
    
    statusBarWidth = width * 0.95
    statusBarHeight = height * 0.07
    
    super.init(frame: CGRect(x: width * 0.025, y: height * 0.005,  width: statusBarWidth,  height: statusBarHeight))
    layer.cornerRadius = getCornerRadius(width: statusBarWidth)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func addElement(elementName name: String, elementValue value: String, labelWidth width: CGFloat, xPosition x: CGFloat, icon i: IconView? = nil) {
    
    if i != nil {
      elementsDictionary[name] = StatusBarElement(elementName: name, elementValue: value, xPosition: x, width: width, height: statusBarHeight, icon: i!)
      elementsOrder.append(name)
    } else {
      elementsDictionary[name] = StatusBarElement(elementName: name, elementValue: value, xPosition: x, width: width, height: statusBarHeight)
      elementsOrder.append(name)
    }
  }
  
  func addLabelsToView() {
    for element in elementsOrder {
      addSubview(elementsDictionary[element]!)
    }
  }
  
  func updateLabel(elementName: String, updatedValue v: Int) {
    elementsDictionary[elementName]!.updateElementValue(value: v)
  }
  
  func dullLabel(elementName: String) {
    elementsDictionary[elementName]!.dullElement()
  }
  
}


