//
//  GameModel.swift
//  buggle
//
//  Created by Benjamin Sparkes on 19/06/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

// The game model has all the information for an instance of a game.

import UIKit


class GameModel: Codable {
  
  private enum CodingKeys: String, CodingKey {
    case time, length, lexicon, tiles,
    dimension, timeRemaining, score,
    foundWords, abstractGameBoard,
    possibleWords, longestWord, longestHeterogram,
    finishedPossibleWordSearch
  }
  
  let length: Int
  let tiles: Int
  let time : Int
  let lexicon : Int
  
  let dimension: Int
  
  var timeRemaining: Double
  var score: Int
  
  var foundWords = [GameWord]()
  var abstractGameBoard = Dictionary<TileIndex, String>()
  
  var possibleWords = [GameWord]()
  var finishedPossibleWordSearch = false
  
  var longestWord = ""
  var longestHeterogram = ""
  
  
  init(time t: Int, timeRemaining tr: Double, length len: Int, tiles til: Int, lexicon lex: Int) {
    
    print("made game model")
    
    time = t
    dimension = til
    length = len
    tiles = til
    lexicon = lex
    timeRemaining = tr
    score = 0
    insertTilesByTileSet(tileSet: tiles)
  }
  
  
  deinit {
    print("DELETED GAME MODEL")
  }
  
  
  func completeWordSetBackground() {
    print("> completeWordSetBackground called")
    
    if finishedTrie {
      
      if (finishedPossibleWordSearch) {
        logw(m: "no need to find more words")
      } else {
        
        let set = self.getCompleteWordSet()
        // Lexicographic sort the possible word set
        self.possibleWords = set.sorted { $0.word.count == $1.word.count ? $0.word < $1.word : $0.word.count < $1.word.count }
        
        logw(m: "finishedPossibleWordSearch")
        self.finishedPossibleWordSearch = true
      }
    } else {
      print("Trie not finished")
    }
  }
  
  
  private func getCompleteWordSet() -> Set<GameWord> {
    print("getCompleteWordSet called")
    
    
    var mem = [TileIndex]()
    var set = Set<GameWord>()
    
    for (tile, _) in abstractGameBoard {
      completeWordSetHelper(tile: tile, memory: &mem, wordSet: &set)
    }
    return set
  }
  
  
  private func completeWordSetHelper(tile: TileIndex, memory: inout [TileIndex], wordSet: inout Set<GameWord>) {
    
    memory.append(tile)
    print("-> Testing word: \(tilelistToWord(tileList: memory))")
    if memory.count >= length && global.wordTrie.memoryContainsWord(word: tilelistToWord(tileList: memory), lexicon: global.gameConfig.lexicon) {
      wordSet.insert(GameWord(word: tilelistToWord(tileList: memory).replacingOccurrences(of: "!", with: "qu")))
    }
    let surroundingTiles = adjoiningWordTiles(location: tile, memory: memory)
    print("-> surrounding tiles: \(surroundingTiles)")
    for stile in surroundingTiles {
      completeWordSetHelper(tile: stile, memory: &memory, wordSet: &wordSet)
    }
    memory.removeLast()
  }
  
  
  // a function process the word
  func processWord(word: String) {
    completeWordSetBackground() // MIGHT NEED TO FIX
    let expandedWord = word.replacingOccurrences(of: "!", with: "qu")
    if global.wordTrie.memoryContainsWord(word: word, lexicon: global.gameConfig.lexicon) && expandedWord.count >= length {
      let wordWrapped = GameWord(word: expandedWord, found: true)
      if !foundWords.contains(wordWrapped) {
        score += wordWrapped.points
        foundWords.append(wordWrapped)
        possibleWords[possibleWords.firstIndex(of: wordWrapped)!].found = true
        //        logw(m: "ratio \(Double(foundWords.count)/Double(possibleWords.count))")
        if expandedWord.count > longestWord.count {
          longestWord = expandedWord
          if heterogramTest(word: expandedWord) { longestHeterogram = expandedWord }
        }
      }
      NotificationCenter.default.post(name: .didFindWord, object: nil, userInfo: ["word" : wordWrapped])
    }
  }
  
  
  // Transform a list of tiles to a word…
  func tilelistToWord(tileList: [TileIndex]) -> String {
    var word = ""
    for tile in tileList {
      word.append((abstractGameBoard[tile]) ?? "-")
    }
    return word
  }
  
  
  private func surroundingTiles(location: TileIndex) -> [TileIndex] {
    let x = location.col
    let y = location.row
    var adjoiningList = [TileIndex]()
    
    // x[y] + 2 as range is half open, allowing dimension unmodified
    for posX in max(0, x - 1) ..< min(x + 2, dimension) {
      for posY in max(0, y - 1) ..< min(y + 2, dimension) {
        if (posX != x || posY != y) {
          adjoiningList.append(TileIndex(row: posY, col: posX))
        }
      }
    }
    return adjoiningList
  }
  
  
  // Get those adjoining tiles which are a possible word completion…
  // Have location and memory.
  // Get accessible list.
  // Construct what the word so far is, and if this is not nil, this means that
  // there are children of the current string in the trie
  // So, look at the children, which is the problem!
  
  private func adjoiningWordTiles(location: TileIndex, memory: [TileIndex]) -> [TileIndex] {
    
    var allowableList = [TileIndex]()
    
    let accessibleList = surroundingTiles(location: location)
    
    // Make sure that there are children and add these to an array if so
    if let trieNodeForWordSoFar = global.wordTrie.memoryGoTo(word: tilelistToWord(tileList: memory)) {
      let possibleContinuingNodes = trieNodeForWordSoFar.getChildren()
      var availableContinuations = [String]()
      for item in possibleContinuingNodes {
        availableContinuations.append(item.getValue())
      }
      // Go through and update the allowable list
      for tile in accessibleList {
        if !memory.contains(where: { $0 == tile }) {
          if availableContinuations.contains(where: { $0 == tilelistToWord(tileList: [tile]) }) {
            allowableList.append(tile)
          }
        }
      }
    }
    return allowableList
  }
  
  
  // Insert a tile to the board…
  private func insertTile(at location: TileIndex, value: String) {
    guard !abstractGameBoard.keys.contains(location) else {
      print("problem")
      return
    }
    abstractGameBoard[location] = value
  }
  
  // Get the letters to be placed on the board
  private func getLettersFromTileSet(set: Int) -> [String] {
    return getTileLettersFromDistribution(tileDistribution: basicDistribution)
  }
  
  
  private func getTileLettersFromDistribution(tileDistribution distribution: [Double]) -> [String] {
    // Distributuion code follows:
    //  https://stackoverflow.com/questions/30309556/generate-random-numbers-with-a-given-distribution
    // This seems better than creating a tree, as it's kind of flipiing an n-sided coin once, rather
    // than flipping a two sided coin n-times, and to flip the two sided coin in a biased way would likely
    // reduce to flipping an n-sided coin.
    
    var selection = [String]()
    let alph = alphabet()
    
    for _ in 1...(dimension*dimension) {
      let number = randomNumber(distribution: basicDistribution)
      let letter = alph.model[number]
      selection.append(letter)
    }
    return selection
  }
  
  // Insert tiles from the tileLetters list
  private func insertTilesByTileSet(tileSet ts: Int) -> Void {
    
    let tileLetters = getLettersFromTileSet(set: ts)
    
    var index = 0
    for i in 0 ..< dimension {
      for j in 0 ..< dimension {
        let location = TileIndex(row: i, col: j)
        let value = tileLetters[index]
        insertTile(at: location, value: value)
        index += 1
      }
    }
  }
  
  
  func saveModel() {
    
    let encoder = PropertyListEncoder()
    let filename = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("model.plist")
    
    do {
      let data = try encoder.encode(self)
      try data.write(to: filename)
      print("saved model to docs")
    } catch {
      print("error writing")
    }
  }
}


func loadModel() -> GameModel? {
  
  let decoder = PropertyListDecoder()
  let filename = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("model.plist")
  var loadedModel: GameModel? = nil
  
  do {
    let data = try Data(contentsOf: filename)
    let decodedModel = try decoder.decode(GameModel.self, from: data)
    deleteModel()
    loadedModel = decodedModel
  } catch {
    print("couldn't load model")
  }
  print("returning")
  return loadedModel
}


func deleteModel() {
  
  let filename = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("model.plist")
  
  do {
    try FileManager.default.removeItem(at: filename)
  } catch {
    print("delete model failed")
  }
}
