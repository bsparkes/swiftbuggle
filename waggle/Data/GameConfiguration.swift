//
//  gameSettings.swift
//  buggle
//
//  Created by Benjamin Sparkes on 18/12/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

// Game configuration is a list of all the settings for a game.

import Foundation

class GameConfiguration: Codable {
  
  private enum CodingKeys: String, CodingKey {
    
    case timeID, lengthID, lexiconID, tilesID,
    timeOptions, lengthOptions, lexiconOptions, tileOptions,
    time, length, lexicon, tiles,
    newTime, newLength, newLexicon, newTiles
  }
  
  public let timeID = "time"
  public let lengthID = "length"
  public let lexiconID = "lexicon"
  public let tilesID = "tiles"
  
  private let timeOptions = [1,3,5,7,-1]
  private let lengthOptions = [3,4,5]
  private let lexiconOptions = [0,1]
  private let tileOptions = [3,4,5,6]
  
  private(set) var time: Int
  private(set) var length: Int
  private(set) var lexicon: Int
  private(set) var tiles: Int
  
  private(set) var newTime: Int
  private(set) var newLength: Int
  private(set) var newLexicon: Int
  private(set) var newTiles: Int
  
  init() {
    time = 3
    length = 3
    lexicon = 0
    tiles = 6
    
    newTime = 3
    newLength = 3
    newLexicon = 0
    newTiles = 6
    print("Finished gameConfig init")
  }
  
  private func lexiconOptionToString(lex: Int) -> String {
    switch lex {
    case 0:
      return "Full"
    default:
      return "Safe"
    }
  }
  
  private func tilesOptionToString(tileOpt: Int) -> String {
    return String(tileOpt) + "\u{00B2}"
  }
  
  public func getOptions(identifer id: String) -> [Int] {
    
    switch id {
    case timeID:
      return timeOptions
    case lengthID:
      return lengthOptions
    case lexiconID:
      return lexiconOptions
    case tilesID:
      return tileOptions
    default:
      return [Int]()
    }
  }
  
  public func getCurrentOptionString(option opt: String, new: Bool) -> String {
    
    switch opt {
    case timeID:
      return new ? String(newTime) : String(time)
    case lengthID:
      return new ? String(newLength) : String(length)
    case lexiconID:
      let lex = new ? newLexicon : lexicon
      return lexiconOptionToString(lex: lex)
    case tilesID:
      let tile = new ? newTiles : tiles
      return tilesOptionToString(tileOpt: tile)
    default:
      logw(m: "Problems with getcurrentoptionsstring")
      return "Error"
    }
  }
  
  public func getCurrentOption(option opt: String, new: Bool) -> Int {
    
    switch opt {
    case timeID:
      return new ? newTime : time
    case lengthID:
      return new ? newLength: length
    case lexiconID:
      return new ? newLexicon : lexicon
    case tilesID:
      return new ? newTiles : tiles
    default:
      return 0
    }
  }
  
  
  public func getOptionString(identifier id: String, value v: Int) -> String {
    
    switch id {
    case timeID:
      return String(v)
    case lengthID:
      return String(v)
    case lexiconID:
      return lexiconOptionToString(lex: v)
    case tilesID:
      return tilesOptionToString(tileOpt: v)
    default:
      logw(m: "error using getOptionsString")
      return "Eroor"
    }
  }
  
  
  public func refreshConfig() {
    //    logw(m: "~~~ Refreshed Game Config ~~~")
    setOption(identifier: lengthID, value: newLength)
    setOption(identifier: timeID, value: newTime)
    setOption(identifier: lexiconID, value: newLexicon)
    setOption(identifier: tilesID, value: newTiles)
  }
  
  // Always update the temp, and also update the actual if no game is running.
  // refreshConfig will then set the temp to the current on a new game, etc.
  public func setOption(identifier id: String, value val: Int) {
    
    switch id {
    case lengthID:
      newLength = val
      if global.gameModel.timeRemaining == 0 {
        length = val
        NotificationCenter.default.post(name: .didChangeLength, object: nil)
      }
    case timeID:
      newTime = val
      if global.gameModel.timeRemaining == 0 {
        time = val
        NotificationCenter.default.post(name: .didChangeTime, object: nil)
      }
    case lexiconID:
      newLexicon = val
      if global.gameModel.timeRemaining == 0 {
        lexicon = val
        NotificationCenter.default.post(name: .didChangeLexicon, object: nil)
      }
    case tilesID:
      newTiles = val
      if global.gameModel.timeRemaining == 0 {
        tiles = val
        NotificationCenter.default.post(name: .didChangeTiles, object: nil)
      }
    default:
      logw(m: "error setting options")
    }
    if global.gameModel.timeRemaining == 0 { NotificationCenter.default.post(name: .didChangeConfig, object: nil) }
  }
  
  public func makeGameModel() -> GameModel {
    return GameModel(time: (time * 60) - 1, timeRemaining: Double((time * 60) - 1), length: length, tiles: tiles, lexicon: lexicon)
  }
  
  public func getConfigIdentifyingString() -> String {
    let seperator = "|"
    return String(time) + seperator + String(length) + seperator + String(lexicon) + seperator + String(tiles)
  }
  
  func saveConfig() -> Void {
    //    print(getConfigIdentifyingString())
    let encoder = PropertyListEncoder()
    let filename = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("gameConfig.plist")
    do {
      let data = try encoder.encode(self)
      try data.write(to: filename)
      Swift.print("gameConfig saved to docs")
    } catch {
      Swift.print("error writing")
    }
  }
}


func loadGameConfig() -> GameConfiguration {
  
  let decoder = PropertyListDecoder()
  let filename = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("gameConfig.plist")
  
  do {
    let data = try Data(contentsOf: filename)
    let decodedConfig = try decoder.decode(GameConfiguration.self, from: data)
    
    print("loaded game config")
    
    return decodedConfig
  } catch {
    print("couldn't load config")
    return GameConfiguration()
  }
}
