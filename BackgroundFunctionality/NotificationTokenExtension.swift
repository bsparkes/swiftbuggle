//
//  NotificationTokenExtension.swift
//  buggle
//
//  Created by Benjamin Sparkes on 22/12/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//
// Many thanks to Ole Begemann, as the following code is directly from
// https://oleb.net/blog/2018/01/notificationcenter-removeobserver/
//

import Foundation

final class NotificationToken: NSObject {
  let notificationCenter: NotificationCenter
  let token: Any
  
  init(notificationCenter: NotificationCenter = .default, token: Any) {
    self.notificationCenter = notificationCenter
    self.token = token
  }
  
  deinit {
    notificationCenter.removeObserver(token)
  }
}


extension NotificationCenter {
  /// Convenience wrapper for addObserver(forName:object:queue:using:)
  /// that returns our custom NotificationToken.
  func observe(name: NSNotification.Name?, object obj: Any?,
               queue: OperationQueue?, using block: @escaping (Notification) -> ())
    -> NotificationToken
  {
    let token = addObserver(forName: name, object: obj, queue: queue, using: block)
    return NotificationToken(notificationCenter: self, token: token)
  }
}


