////
////  tries.swift
////  buggle
////
////  Created by Benjamin Sparkes on 19/06/2018.
////  Copyright © 2018 Benjamin Sparkes. All rights reserved.
////
//// with help from https://www.raywenderlich.com/139410/swift-algorithm-club-swift-trie-data-structure
//// the lowering the case of words isn't really necessary for my use, but it would be cool to allow for the wordTrie to be updated, so it might come in handy.
//
//import Foundation
//
//// For testing loading
//import AVFoundation
//import UIKit
//
//// wordTypes:
//// 0 = safe
//// 1 = unsafe
//
//class TrieNode {
//  
//  let value: Character
//  var children = [TrieNode]()
//  var isTerminating: Bool = false
//  var wordType: Int = 0
//  
//  init(value v: Character) {
//    self.value = v
//  }
//  
//  func add(char: Character) -> TrieNode {
//    for child in children {
//      if child.value == char {
//        return child
//      }
//    }
//    let newNode = TrieNode(value: char)
//    children.append(newNode)
//    return newNode
//  }
//}
//
//class Trie {
//  
//  typealias Node = TrieNode
//  fileprivate let root: TrieNode
//  
//  init() {
//    root = Node(value: "@")
//  }
//  
//  
//  init(fName: String) {
//    root = Node(value: "@")
//    insertFromFile(fName: fName)
//  }
//  
//  func insertFromFile(fName: String) {
//    
//    if let path = Bundle.main.path(forResource: fName, ofType: "txt") {
//      do {
//        let data = try String(contentsOfFile: path, encoding: .utf8)
//        let myStrings = data.components(separatedBy: .newlines)
//        for word in myStrings {
//          self.insert(word: word)
//        }
//      } catch {
//        print("error")
//      }
//    }
//    let generator = UIImpactFeedbackGenerator(style: .heavy)
//    generator.impactOccurred()
//    logw(m: "trie built using insert")
//  }
//  
//  func backgroundInsertFromFile(fName: String) {
//    
//    DispatchQueue.global(qos: .userInitiated).async {
//      logw(m: "loading trie")
//      self.insertFromFile(fName: fName)
//      DispatchQueue.main.async {
//        let generator = UIImpactFeedbackGenerator(style: .heavy)
//        generator.impactOccurred()
//        logw(m: "trie built using background insert")
//      }
//    }
//  }
//  
//  
//  func insert(word: String) {
//    guard !word.isEmpty else { return }
//    var currentNode = root
//    let lword = word.lowercased()
//    for i in 0 ..< lword.count {
//      currentNode = currentNode.add(char: lword[lword.index(lword.startIndex, offsetBy: i)])
//    }
//    currentNode.isTerminating = true
//    if word.last!.isUppercase {
//      currentNode.wordType = 1
//    }
//  }
//  
//  
//  func containsWord(word: String) -> Bool {
//    let endNode = goto(word: word)
//    return endNode != nil && endNode!.isTerminating
//  }
//  
//  func containsWord(word: String, lexicon: Int) -> Bool {
//    let endNode = goto(word: word)
//    if endNode != nil && endNode!.isTerminating && !(endNode!.wordType <= lexicon) {
//      print("Naughty!")
//    }
//    return endNode != nil && endNode!.isTerminating && endNode!.wordType <= lexicon
//  }
//  
//  
//  func goto(word: String) -> Node? {
//    // Go through the trie tracing the letters.
//    // At a given point in the string it looks for the next
//    // character as a child and moves
//    var currentNode = root
//    let lword = word.lowercased()
//    guard word.count > 0 else { return root }
//    for i in 0 ..< lword.count {
//      let index = lword.index(lword.startIndex, offsetBy: i)
//      var nextNode : TrieNode? = nil
//      for child in currentNode.children {
//        if child.value == lword[index] {
//          nextNode = child
//          break
//        }
//      }
//      if nextNode == nil {return nil}
//      currentNode = nextNode!
//    }
//    return currentNode
//  }
//  
//  
////  func ancestry(node: TrieNode) -> String { //<String>) -> String {
////    var currentNode = node
////    var ancestors = [String]()
////    guard currentNode.value != nil else { return ""}
////    while currentNode.value != nil {
////      ancestors.insert(String(currentNode.value!), at: 0)
////      currentNode = currentNode.parent!
////    }
////    return ancestors.joined()
////  }
//}
//
////func buildTrie(fName: String, trie: Trie) {
////  if let path = Bundle.main.path(forResource: fName, ofType: "txt") {
////    do {
////      let data = try String(contentsOfFile: path, encoding: .utf8)
////      let myStrings = data.components(separatedBy: .newlines)
////      for word in myStrings {
////        trie.insert(word: word)
////      }
////    } catch {
////      print("error")
////    }
////  }
////}
//
//
//
//
//
