//
//  coreTrie.swift
//  waggle
//
//  Created by Ben Sparkes on 13/03/2020.
//  Copyright © 2020 sparkes. All rights reserved.
//

import UIKit
import CoreData

class TrieNode {
  private let value: String
  private var wordType: Int16 = 0
  private var children = Set<TrieNode>()
  private var coreChildren = Set<CoreTrieNode>()
  
  init(value: String) {
    self.value = value
  }
  
  init(value: String, wordType: Int16, coreChildren: Set<CoreTrieNode>) {
    self.value = value
    self.wordType = wordType
    self.coreChildren = coreChildren
  }
  
  init(value: String, wordType: Int16, children: Set<TrieNode>, coreChildren: Set<CoreTrieNode>) {
    self.value = value
    self.wordType = wordType
    self.children = children
    self.coreChildren = coreChildren
  }
  
  func addCoreDataChildren() {
    print("adding children for \(self.value)")
    print("existing children: ", self.children)
    print("Core children: \(self.coreChildren)")
    for coreChild in self.coreChildren {
      self.children.insert(TrieNode(value: coreChild.value, wordType: coreChild.wordType, coreChildren: coreChild.children))
      self.coreChildren.removeAll()
    }
  }
  
  func getValue() -> String {
    return self.value
  }
  
  func getWordType() -> Int16 {
    return self.wordType
  }
  
  func getChildren() -> Set<TrieNode> {
    
    if self.children.isEmpty {
      self.addCoreDataChildren()
    }
    return self.children
  }
  
  func getCoreChildren() -> Set<CoreTrieNode> {
    return self.coreChildren
  }
  
  
  func setChildren(set: Set<TrieNode>) {
    self.children = set
  }
  
  func setCoreChildren(set: Set<CoreTrieNode>) {
    self.coreChildren = set
  }
  
  func trieNodeGoToChild(value: String) -> TrieNode? {
    print("Going from \(self.value) to child \(self.value)")
    
    if self.children.isEmpty {
//      print("--- Loading nodes from core data ---")
      self.addCoreDataChildren()
    }
    for child in self.children {
      if child.value == value {
        return child
      }
    }
    return nil
  }
}

extension TrieNode: Equatable {
  static func == (lhs: TrieNode, rhs: TrieNode) -> Bool {
    return lhs.value == rhs.value && lhs.wordType == rhs.wordType && lhs.children == rhs.children && lhs.coreChildren == rhs.coreChildren
  }
}

extension TrieNode: Hashable {
  func hash(into hasher: inout Hasher) {
    hasher.combine(value)
    hasher.combine(wordType)
    hasher.combine(children)
    hasher.combine(coreChildren)
  }
}

class CoreTrie {
  
  
  
  let nodeDescription: NSEntityDescription
  var coreDataRoot: CoreTrieNode
  let memoryRoot = TrieNode(value: "@")
  let coreTrieContext: NSManagedObjectContext
  
  init() {
    print("!>! core trie init called !<!")
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//    deleteAllCoreData(entity: "CoreTrieNode")
    coreTrieContext = appDelegate.persistentContainer.viewContext
    print("context set")

    self.nodeDescription = NSEntityDescription.entity(forEntityName: "CoreTrieNode", in: coreTrieContext)!
    self.coreDataRoot = NSManagedObject(entity: nodeDescription, insertInto: coreTrieContext) as! CoreTrieNode
    self.coreDataRoot.setValue("@", forKey: "value")
    self.coreDataRoot.parent = nil
    print("settings core root")
    if let coreRoot = findCoreTrieRoot() {
      print(">>> found core trie root <<<")
      self.memoryRoot.setCoreChildren(set: coreRoot.children)
    } else {
      print(">!> core trie root NOT found <!<")
      insertToCoreDataFromFile(fName: "updatedList")
      let coreRoot = findCoreTrieRoot()
      self.memoryRoot.setCoreChildren(set: coreRoot!.children)
    }
    print("memory Root core children count: ", self.memoryRoot.getCoreChildren().count)
    finishedTrie = true
  }
  
  func ping() -> Bool {
    print("memory Root core children count: ", self.memoryRoot.getCoreChildren().count)
    return true
  }
  
  
  func memoryGoTo(word: String) -> TrieNode? {
    print("memoryGoTo word \(word)")
    // Go through the trie tracing the letters.
    // At a given point in the string it looks for the next
    // character as a child and moves
    var currentNode: TrieNode? = self.memoryRoot
    let lword = word.lowercased()
    guard word.count > 0 else { return self.memoryRoot }
    for i in 0 ..< lword.count {
      let index = lword.index(lword.startIndex, offsetBy: i)
      currentNode = currentNode!.trieNodeGoToChild(value: String(lword[index]))
      if currentNode == nil {
        print("-- \(word) not found")
        break
//        return nil
      }
    }
    print("-- \(word) found!")
    return currentNode
  }
  
  func memoryContainsWord(word: String, lexicon: Int) -> Bool {
    print("checking: ", word)
    let endNode = memoryGoTo(word: word)
    print("And: ", endNode != nil && endNode!.getWordType() > 0)
    print("Is there are node? ", endNode != nil)
    return endNode != nil && endNode!.getWordType() > lexicon
  }
  
  
  private func findCoreTrieRoot() -> CoreTrieNode? {
    print("finding core trie root")
    
    var result = [Any]()
    var root: CoreTrieNode? = nil
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CoreTrieNode")
    
    
    do {
      result = try coreTrieContext.fetch(fetchRequest)
      print("got fetch request")
    } catch {
      print("Failed to obtain context")
    }
    for node in result {
      let nnode = node as! CoreTrieNode
      if nnode.value == "@" {
        print("found root")
        if nnode.children.count != 0 {
          root = nnode
        }
      }
    }
    return root
  }
  
  private func createCoreDataNode(parent: CoreTrieNode, value: String) -> CoreTrieNode {
    let node = NSManagedObject(entity: nodeDescription, insertInto: coreTrieContext) as! CoreTrieNode
    node.setValue(value, forKey: "value")
    node.parent = parent
    return node
  }
  
  private func addCoreDataNode(parent: CoreTrieNode, value: String) -> CoreTrieNode {
    for child in parent.children {
      if child.value == value {
        return child
      }
    }
    let newNode = createCoreDataNode(parent: parent, value: value)
    return newNode
  }
  
  private func addCoreDataWord(word: String) {
    guard !word.isEmpty else { return }
    var currentNode = self.coreDataRoot
    let lword = word.lowercased()
    for i in 0 ..< lword.count {
      currentNode = self.addCoreDataNode(parent: currentNode, value: String(lword[lword.index(lword.startIndex, offsetBy: i)]))
    }
    currentNode.wordType = word.last!.isUppercase ?  1 : 2
  }
  
//  private func checkCoreDataWord(word: String) -> Bool {
//    print("checking: ", word)
//    let endNode = coreDataGoTo(word: word)
//    return endNode != nil && endNode!.wordType > 0
//  }
  
//  private func coreDataContainsWord(word: String, lexicon: Int) -> Bool {
//    print("checking: ", word)
//    let endNode = coreDataGoTo(word: word)
//    if endNode != nil && endNode!.wordType > 0 {
//      print("Naughty!")
//    }
//    print("And: ", endNode != nil && endNode!.wordType > 0)
//    print("Is there are node? ", endNode != nil)
//    return endNode != nil && endNode!.wordType > 0
//  }
  
//  private func coreDataGoTo(word: String) -> CoreTrieNode? {
//    print("Core Data-ng word: \(word)")
//    // Go through the trie tracing the letters.
//    // At a given point in the string it looks for the next
//    // character as a child and moves
//    var currentNode = self.coreDataRoot
//    let lword = word.lowercased()
//    guard word.count > 0 else { return self.coreDataRoot }
//    for i in 0 ..< lword.count {
//      let index = lword.index(lword.startIndex, offsetBy: i)
//      var nextNode : CoreTrieNode? = nil
//      for child in currentNode.children {
//        if child.value == String(lword[index]) {
//          //          print("Going to \(lword[index])")
//          nextNode = child
//          break
//        }
//      }
//      if nextNode == nil {return nil}
//      currentNode = nextNode!
//    }
//    return currentNode
//  }
  
//  private func displayFromCoreDataNode(node: CoreTrieNode) {
//    for child in node.children {
//      if child.value == "@" { print("Oh"); return }
//      print(child.value)
//      displayFromCoreDataNode(node: child)
//    }
//  }
  
//  func displayFromCoreDataRoot() {
//    displayFromCoreDataNode(node: self.coreDataRoot)
//  }
  
  private func insertToCoreDataFromFile(fName: String) {
    
    if let path = Bundle.main.path(forResource: fName, ofType: "txt") {
      do {
        let data = try String(contentsOfFile: path, encoding: .utf8)
        let myStrings = data.components(separatedBy: .newlines)
        for word in myStrings {
          self.addCoreDataWord(word: word)
        }
      } catch {
        print("error")
      }
    }
    
    let generator = UIImpactFeedbackGenerator(style: .heavy)
    generator.impactOccurred()
    logw(m: "trie built using insert")
    finishedTrie = true
    
    do {
      try coreTrieContext.save()
      print("!!! SAVED !!!")
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
    print("Finished insertion from \(fName)")
  }
  
  func checkWordsFromFile(fName: String) {
    
    if let path = Bundle.main.path(forResource: fName, ofType: "txt") {
      do {
        let data = try String(contentsOfFile: path, encoding: .utf8)
        let myStrings = data.components(separatedBy: .newlines)
        for word in myStrings {
          if self.memoryGoTo(word: word) != nil {
//            print(word)
          } else {
//            print("missing \(word)")
          }
        }
        print("Check completed")
      } catch {
        print("error")
      }
    }
  }
}

func deleteAllCoreData(entity: String) {
  print("Deleting all data for entity: \(entity)")
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  let managedContext = appDelegate.persistentContainer.viewContext
  let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
  fetchRequest.returnsObjectsAsFaults = false
  do {
    let results = try managedContext.fetch(fetchRequest)
    for managedObject in results {
      let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
      managedContext.delete(managedObjectData)
    }
  } catch let error as NSError {
    print("Delele all data in \(entity) error : \(error) \(error.userInfo)")
  }
  print("Finished deletion")
}




func testOhio() {
  // I think the idea is to create the trie structure in core data, and then fetch nodes as required.
  // In this way, only fetch what is needed, with the root always loaded at the start.
  // Whatever if fetched, then, stays in memory.
  // This works on the assumption that fetching works the same in all cases, which I *think* is true.
  // As things stand I'd need to fetch all children, but this might be fine.
  // https://developer.apple.com/documentation/coredata/nsmanagedobjectcontext/1506686-existingobject
  
  //  deleteAllCoreData(entity: "CoreTrieNode")
  
  //  print("Root children: \(wordTrie.showRoot().children)")
  //  print("Root child type", wordTrie.showRoot().children.description)
//  wordTrie.checkWordsFromFile(fName: "updatedList")
}
