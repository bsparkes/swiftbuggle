
def loadWords(file):

  with open(file, 'r') as f:
      x = f.read().split('\n')
      return x

def processBadList(wordList):
  listLegnth = len(wordList)
  index = 0
  while index < listLegnth:
    if " " in wordList[index] or not wordList[index].isalpha():
      del wordList[index]
      listLegnth -= 1
    else:
      index += 1

  pluralList = []
  for word in wordList:
    pluralList.append(word + "s")
    pluralList.append(word + "es")
  wordList = wordList + pluralList
  return wordList

def markBad(badList, wordList):
  for index in range(len(wordList)):
    if wordList[index] in badList:
      word = wordList[index]
      wordList[index] = word[:-1] + word[-1].upper()
  return wordList

def replaceQu(wordList):
  for index in range(len(wordList)):
    if "qu" in wordList[index]:
      wordList[index] = wordList[index].replace('qu', '!')

  return wordList

def saveList(file, wordList):

  with open(file, 'w') as d:
    for word in wordList:
      if len(word) > 0:
        d.write(word)
        d.write('\n')

def toNumbers(file, wordList):
  with open(file, 'w') as d:
    for word in wordList:
      if len(word) > 0:
        for char in word:
          # print(char)
          d.write(str(ord(char)))
        d.write('\n')

def ensureStrings(list):
  for word in list:
    if len(word) > 0:
      word = str(word)
  return wordList

badList = loadWords('badwords.txt')
badList = processBadList(badList)
wordList = loadWords("wordlist.txt")
wordList = markBad(badList, wordList)
wordList = replaceQu(wordList)
# wordList = ensureStrings(wordList)

# toNumbers('numList.txt', wordList)